
\section{Eisenstein series calculations}
\label{sec:eisenst-series-calc}

In this section we will define Eisenstein series for $M_{k,L}$ and calculate their Fourier coefficients, leading to Theorem~\ref{thm:C-gamma-n-general-k}.
The calculations will only be done for the case $2k - b^{-} + b^{+} = \epsilon \equiv 0 \pmod 4$, the other case $\epsilon \equiv 2 \pmod 4$ can be treated in the same way.
We are going to use the results of this section for Eisenstein series of weight $k = 1 + \frac{l}{2}$ and lattices of signature $(2,l)$, so $\epsilon = 4$, and the treated case is the interesting one.

Let $\beta \in L'/L$ with $q(\beta) \in \ZZ$. If we consider the vector $\ee_{\beta} \in \CLL$ as a constant function on $\HH$, it is invariant under the action of $T$ and $Z^{2} \in \mpt{\ZZ}$.

\begin{Def}
  The Eisenstein series for the lattice $L$ and $\beta \in L'/L$ is defined as
  \begin{equation}
    \label{eq:eisensteinDef}
    E_{\beta} (\tau) = \frac{1}{2} \sum_{(M,\phi) \in \tilde \Gamma_{\infty} \backslash \mpt{\ZZ}} \ee_{\beta} \big|_{k}^{*} (M,\phi)
  \end{equation}
It converges normally on $\HH$ and $E_{\beta} \in M_{k,L}$. The $S$-invariance follows easily from the fact that
\[ \big( f \big|_{k}^{*} M_{1} \big) \big|_{k}^{*} M_{2} = f \big|_{k}^{*} (M_{1} M_{2})  \] 
and that the sum goes over all elements in $\tilde \Gamma_{\infty} \backslash \mpt{\ZZ}$.
\end{Def}

\subsection[Fourier coefficients]{Fourier coefficients \texorpdfstring{$C_{\beta}(\gamma,n)$}{Cᵦ(ɣ,n)}}
\label{sec:fourier-coeff-C}
\begin{Prop}
  The coefficients of the Fourier expansion
  \begin{equation*}
    E_{\beta} (\tau) = \sum_{\gamma \in L'/L} \sum_{\substack{n \in \ZZ - q(\gamma) \\ n \geq 0}} C_{\beta}(\gamma,n) \ee_{\gamma}(n\tau)
  \end{equation*}
are
\begin{equation}
  \label{eq:CKlost}
  C_{\beta} (\gamma,n) =
  \begin{dcases}
    \delta_{\beta,\gamma} + \delta_{-\beta,\gamma} & \text{if $n=0$} \\
    \frac{(2\pi)^{k}n^{k-1}}{\Gamma(k)} \sum_{c \in \ZZ \setminus \{0\}} |c|^{1-k} H_{c}^{*}(\beta,0,\gamma,n) & \text{if $n>0$}
  \end{dcases}
\end{equation}
$H_{c}^{*}(\beta ,t ,\gamma,n)$ is the \emph{generalized Kloosterman sum}
\begin{equation}
  \label{eq:KloostermanSum}
  H_{c}^{*}(\beta ,t ,\gamma,n) = \frac{e^{-\pi i \sgn(c) k /2}}{|c|} \;\;
      \sum_{\mathclap{\substack{d (c)^{*} \\ \sabcd \in \Gamma_{\infty} \backslash \Gamma_{1} \slash \Gamma_{\infty} }}} \;\;
      \rho_{\beta\gamma}\widetilde{\abcd} \, e \left( \frac{ta+nd}{c} \right)
\end{equation}
where the sum $\sum_{d (c)^{*}}$ is over all primitive residues $d$ modulo $c$.
\end{Prop}
Observe for $m,n \in \ZZ$
\begin{IEEEeqnarray}{rCl}
  \abcd \begin{pmatrix}
1    & n \\ 0 & 1
  \end{pmatrix} & = & \begin{pmatrix}
a    & an+b \\ c & cn+d
  \end{pmatrix}
\quad \text{and} \label{eq:matrix-prod-MTn} \\
\begin{pmatrix}
 1 & m \\ 0 & 1
\end{pmatrix}
\abcd & = & \begin{pmatrix}
 a+mc & b+md \\ c & d
\end{pmatrix}. \label{eq:matrix-prod-TmM}
\end{IEEEeqnarray}
So a double coset representative of $\Gamma_{\infty} \backslash \Gamma_{1} \slash \Gamma_{\infty}$ is given by
$\sabcd \in \Gamma_{1}$ with lower row $(c \,\,\, d)$ and $d \in (\ZZ / c\ZZ)^{*}$.
The summand $ \rho_{\beta\gamma}\widetilde{\sabcd} \, e \left( \frac{ta+nd}{c} \right)$ does not depend on the choice of the coset representative.
\begin{proof}[Proof of the proposition]
  We follow the calculation from \cite[Theorem 1.6, \pno 24]{Bruinier2002}.
  We calculate the Fourier coefficients $C_{\beta}(\gamma,n)$ via \eqref{eq:FEintegral}, so
\[ C_{\beta}(\gamma,n) = \frac{1}{2} \int_{0}^{1} 
    \left\langle \sum_{(M,\phi) \in \tilde \Gamma_{\infty} \backslash \mpt{\ZZ}} \ee_{\beta} \big|_{k}^{*}(M,\phi) ,
      \ee_{\gamma}(n \bar \tau) \right\rangle \dd x \]
Using the fact that $\ee_{\beta}$ is invariant under the action of $Z^{2}$ (so we can sum over $\Gamma_{\infty} \backslash \slt{\ZZ}$, twice), this equals
\begin{equation}
  \label{eq:Cproof1}
  C_{\beta}(\gamma,n) =  \sum_{M \in \Gamma_{\infty} \backslash \slt{\ZZ}} 
    \int_{0}^{1} \left\langle \ee_{\beta} \big|_{k}^{*} \tilde M ,
      \ee_{\gamma}(n \bar \tau) \right\rangle \dd x
\end{equation}
Now we use \eqref{eq:matrix-prod-MTn} and combine all the integrals for one right-$\Gamma_{\infty}$-class of every element $M$ of $\Gamma_{\infty} \backslash \slt{\ZZ}$ to get
\begin{equation*}
  C_{\beta}(\gamma,n) = \delta_{0,n} \big( \delta_{\beta,\gamma} + \delta_{-\beta,\gamma} \big) +
  \sum_{\mathclap{\substack{c \neq 0 \\ \sabcd \in \Gamma_{\infty} \backslash \Gamma_{1} \slash \Gamma_{\infty}}}}
    \;\; \int_{-\infty}^{\infty} (c \tau + d)^{-k} \left\langle \rho_{L}^{*} \widetilde \sabcd ^{-1} \ee_{\beta}, \ee_{\gamma}(n \bar \tau) \right\rangle,
\end{equation*}
where we already evaluated the integral in \eqref{eq:Cproof1} for the left-$\Gamma_{\infty}$-class of the identity matrix and $Z$ to $ \delta_{0,n} ( \delta_{\beta,\gamma} + \delta_{-\beta,\gamma} )$.

Observe that
\begin{equation*}
  \left\langle \rho_{L}^{*} \widetilde \sabcd ^{-1} \ee_{\beta}, \ee_{\gamma}(n \bar \tau) \right\rangle =
    \rho_{\beta \gamma} \widetilde \sabcd e(-n \tau)
\end{equation*}
because $\rho_{L}$ is unitary. Thus $C_{\beta}(\gamma,n)$ equals
\begin{equation}
  \label{eq:Cproof2}
  \sum_{\mathclap{\substack{c \neq 0 \\ \sabcd \in \Gamma_{\infty} \backslash \Gamma_{1} \slash \Gamma_{\infty}}}}
    \;\; \rho_{\beta \gamma} \widetilde \sabcd \int_{-\infty}^{\infty} (c \tau + d)^{-k} e(-n \tau) \dd x.
\end{equation}
Now we calculate the integral. It is
\[ \sqrt{c \tau + d} = \sgn (c) \sqrt{c} \sqrt{\tau + \tfrac{d}{c}} \]
and the integral becomes
\begin{IEEEeqnarray*}{rCl}
  \IEEEeqnarraymulticol{3}{l}{
    \int_{-\infty}^{\infty} (c \tau + d)^{-k} e(-n \tau) \dd x
  } \\ \qquad
    & = & |c|^{-k} \sgn(c)^k \int_{-\infty}^{\infty} (\tau + \tfrac{d}{c})^{-k} e(-n \tau) \dd x \\
    & = & |c|^{-k} \sgn(c)^k e \left( \frac{nd}{c} \right) \int_{-\infty}^{\infty} \tau^{-k} e(-n \tau) \dd x .
\end{IEEEeqnarray*}
Now substituting $\tau = i w$, this equals to
\[ 2 \pi i^{-k} |c|^{-k} \sgn(c)^k e \left( \frac{nd}{c} \right) \frac{1}{2 \pi i} \int_{C - i\infty}^{C + i\infty} w^{-k} e^{2\pi n w} \dd w \]
with $C>0$. For non-positive $n$ this integral tends to $0$ as $C \rightarrow \infty$, so in this case
\[ C_{\beta}(\gamma,n) = \delta_{0,n} \big( \delta_{\beta, \gamma} + \delta_{-\beta, \gamma} \big). \]

For positive $n$ this is an inverse Laplace transform which evaluates as
\begin{equation*}
  \frac{1}{2 \pi i} \int_{C - i\infty}^{C + i\infty} w^{-k} e^{2\pi n w} \dd w =
      \frac{(2 \pi n)^{k-1}}{\Gamma(k)}
\end{equation*}
according to \cite[\pno 238 (1)]{Erdelyi1954}. One can also deform the path of this integral to obtain the Hankel-integral of $\frac{1}{\Gamma(k)}$, see~\cite{Schmelzer2007}.

Insert this into \eqref{eq:Cproof2} and write $i^{-k}\sgn(c)^{k} = e^{-\pi i \sgn(c) k /2}$ to obtain
\begin{equation*}
  C_{\beta}(\gamma,n) = \frac{(2\pi)^{k}n^{k-1}}{\Gamma(k)} \;\;
      \sum_{\mathclap{\substack{c \neq 0 \\ \sabcd \in \Gamma_{\infty} \backslash \Gamma_{1} \slash \Gamma_{\infty}}}} \;\; |c|^{-k} e^{- \pi i \sgn(c) k /2}
      \rho_{\beta \gamma} \widetilde \sabcd e \left( \frac{nd}{c} \right)
\end{equation*}
which is just the assertion after inserting definition \eqref{eq:KloostermanSum} of $H_{c}^{*}(\beta, 0, \gamma, n)$.
\end{proof}

\begin{Lem}
  The series \eqref{eq:CKlost} for $C_{\beta}(\gamma,n)$ converges absolutely.
\end{Lem}
\begin{proof}
  Because of Lemma~\ref{lem:weilRepFinitGr} the coefficients $\rho_{\beta\gamma}\widetilde{\sabcd}$ are universally bounded.
Hence the sum $H_{c}^{*}(\beta ,t ,\gamma,n)$ is universally bounded as well for all $\gamma \in L'/L$, $n \in \ZZ - q(\gamma)$ and $c \in \ZZ \setminus \{0\}$, as the sum is finite, the exponential terms only take values of modulus $1$ and the dependence on $|c|$ is reciprocal.
So for fixed $\beta, \gamma, n$ the sum in \eqref{eq:CKlost} is
\[ \mathcal{O}\left( \sum_{c \in \ZZ \setminus \{0\}} |c|^{-k} \right) \]
as $c$ runs through the non-zero integers, which converges because $k > 2$.
\end{proof}
Throughout the rest of the thesis we will only be interested in the Eisenstein series $E_{0}(\tau)$ which we abbreviate $E(\tau)$.
We also abbreviate $C(\gamma,n) := C_{0}(\gamma,n)$.

Now we deduce more explicit formulas for $C(\gamma,n)$ for later applications in the last chapter.
\begin{Lem}\label{lem:KloostermanSum-Moebius-N}
  The generalized Kloosterman sum $H_{c}^{*}(0 ,0 ,\gamma,n)$ equals 
  \begin{equation*}
    \frac{(-1)^{\epsilon/4}}{\sqrt{|L'/L|}} |c|^{-1+m/2} \sum_{a|c} a^{1-m} \mu(|c|/a) N_{\gamma,n}(a)
  \end{equation*}
with $\mu$ the Moebius function and 
\begin{equation}
  \label{eq:N_gamma-a}
  N_{\gamma,n}(a) = \# \setb{r \in L/aL}{q(r-\gamma) +n \equiv 0 \pmod{a}}.
\end{equation}
Observe
\[ q(r-\gamma) = \underbrace{q(r)}_{\in \ZZ/ a\ZZ} - \underbrace{(r,\gamma)}_{\in \ZZ/ a\ZZ} + \underbrace{q(r) +n}_{\in \ZZ} \]
so the left hand side of the congruence in \eqref{eq:N_gamma-a} is always integral modulo $a$.
\end{Lem}
\begin{proof}
  We plug in the formula for the $(0,\gamma)$-coefficient $\rho_{0,\gamma}$ from \ref{prop:Shintani} into the Definition \eqref{eq:KloostermanSum} of the $H_{c}^{*}$ and see that $H_{c}^{*}(0 ,0 ,\gamma,n)$ equals
\[ \frac{e^{-\pi i \sgn(c) \epsilon /4}}{\sqrt{|L'/L|} |c|^{1+m/2}}
       \sum_{\substack{d \ (c)^{*} \\ ad \equiv 1 \ (c)}} \sum_{r \in L/cL}
          e \left( \frac{a q(r) - (\gamma,r) + d q(\gamma) + nd}{c} \right) \]
Note that for a given $c$ and $d$ a primitive residue modulo $c$, we can choose a double coset representative for which we have $ad = 1 + bc$, so $ad \equiv 1 \pmod{c}$, and the sum $\sum_{d \ (c)^{*},\, ad \equiv 1 \ (c)}$
is in fact over the same elements as the sum in \eqref{eq:KloostermanSum}.

Now a further modulo $c$ calculation shows that this equals
\begin{equation*}
  \frac{(-1)^{\epsilon /4}}{\sqrt{|L'/L|} |c|^{1+ m/2}} \sum_{r \in L/cL} \sum_{d\ (c)^{*}} 
      e \left( \frac{d q(r-\gamma) +n}{c} \right).
\end{equation*}
\cite[Theorem 8.6]{Apostol1976} evaluates the Ramanujan sum in terms of the Moebius function:
\begin{equation}
  \label{eq:RamanujanSumMoeb}
  \sum_{d\ (c)^{*}} e \left( \frac{dn}{c} \right) = \sum_{a | (c,n)} \mu \left( \frac{|c|}{a} \right)a.
\end{equation}
This gives us
\[ H_{c}^{*}(0 ,0 ,\gamma,n) = \frac{(-1)^{\epsilon/4}}{\sqrt{|L'/L|} |c|^{1+ m/2}}
      \sum_{a | c} \mu \left( \frac{|c|}{a} \right) a \;\; \sum_{\mathclap{\substack{r \in L/ cL \\ q(r-\gamma)+n \equiv 0 (a)}}} \ 1. \]

The condition 
\[   q(r-\gamma)+n \equiv 0 \pmod{a} \]
only depends on $r \bmod aL$, so the counting sum over $L/cL$ may be replaced with one over $r/aL$, which then equals to $N_{\gamma,n}(a)$ from \eqref{eq:N_gamma-a}, and we get a factor of $\bigl( \frac{|c|}{a} \bigr)^{m}$. Summed up, we have
\[ H_{c}^{*}(0 ,0 ,\gamma,n) = \frac{(-1)^{\epsilon/4}}{\sqrt{|L'/L|}} |c|^{-1 + m/2}
        \sum_{a | c} \mu \left( \frac{|c|}{a} \right) a^{1-m} N_{\gamma,n}(a). \qedhere \]
\end{proof}

\begin{Prop}
  \label{prop:C-L-series}
  Let $\gamma \in L'$ and $n \in \ZZ - q(r),\, n>0$. The Eisenstein coefficient $C(\gamma,n)$ equals the value at $s=k$ of the analytic continuation in $s$ of 
  \begin{equation}
    \label{eq:C-L-series}
    \frac{2^{k+1} \pi^{k} n^{k-1} (-1)^{\epsilon/4}}{\sqrt{|L'/L|} \Gamma(k) \zeta(s - m/2)} L_{\gamma,n}(s).
  \end{equation}
$\zeta$ is the Riemann $\zeta$-function and
\begin{equation}
  \label{eq:L-series-def}
  L_{\gamma,n} (s) = \sum_{a \geq 1} N_{\gamma,n} (a) \ a^{1-m/2-s}.
\end{equation}
\end{Prop}
\begin{proof}
  Let
  \begin{equation}\label{eq:L-tilde-def}
    \tilde L_{\gamma,n} (s) = \; \sum_{\mathclap{c \in \ZZ \setminus \{0\} }} \ |c|^{1-s} H_{c}^{*}(0 ,0 ,\gamma,n)
  \end{equation}
which is a normally convergent $L$-series for $\Re(s) > 2$. We plug in the formula for $H_{c}^{*}$ from Lemma~\ref{lem:KloostermanSum-Moebius-N} and get
\begin{equation*}
  \tilde L_{\gamma,n}(s) = \frac{2 (-1)^{\epsilon/4}}{\sqrt{|L'/L|}} \sum_{c \geq 1} c^{m/2-s}
        \sum_{a|c} \mu \left( \frac{c}{a} \right)\, a^{1-m} N_{\gamma,n}(a)
\end{equation*}
We substitute $d= \frac{c}{a}$ and see
\begin{IEEEeqnarray}{rCl}
  \tilde L_{\gamma,n} (s) & = & \frac{2 (-1)^{\epsilon/4}}{\sqrt{|L'/L|}} 
        \sum_{c \geq 1} \sum_{a|c} \mu \left(  \frac{c}{a} \right) \left( \frac{c}{a} \right)^{m/2-s} a^{1-m/2-s} N_{\gamma,n} (a) \nonumber \\
      & = & \frac{2 (-1)^{\epsilon/4}}{\sqrt{|L'/L|}} \left( \sum_{d \geq 1} \mu(d)d^{m/2-s} \right)
          \left( \sum_{a \geq 1} a^{1-m/2 -s} N_{\gamma,n}(a) \right) \nonumber \\
      & = & \frac{2 (-1)^{\epsilon/4}}{\sqrt{|L'/L|} \zeta(s-m/2)} L_{\gamma,n} (s) \label{eq:L-tilde-L}
\end{IEEEeqnarray}
using that
\[ \frac{1}{\zeta(s)} = \sum_{d \geq 1} \mu(d) d^{-s}. \]
Inserting the definition \eqref{eq:L-tilde-def} of $\tilde L_{\gamma,n}$ into equation \eqref{eq:CKlost} yields
\[ C(\gamma,n) = \frac{(2\pi)^{k} n^{k-1}}{\Gamma(k)} \tilde L_{\gamma,n}(k). \]
Now using \eqref{eq:L-tilde-L} this equals (after analytic continuation) to the assertion \eqref{eq:C-L-series}.
\end{proof}
The $L$-series $L_{\gamma,n}(s)$ in \eqref{eq:L-series-def} only converges for $\Re(s) > 1 + m/2$.
But the equality of \eqref{eq:L-tilde-def} (converging for $\Re(s) > 2$) and \eqref{eq:L-tilde-L} and the fact that $\zeta(s)$ has a simple pole at $s=1$ gives us a meromorphic continuation of $L_{\gamma,n}(s)$ to $\Re(s) > 2$ with a simple pole at $1+m/2$.