\section{\texorpdfstring{$\CC[L'/L]$}{C[L'/L]}-valued modular forms}
\label{sec:CLL-modular-forms}

In the following let $f$ be a \CLL -valued function on $\HH$ and $k \in \frac{1}{2} \ZZ$.
\begin{Def}
  The \emph{Petersson slash operator} for $(M,\phi) \in \mpt{\ZZ}$ is
  \[ \bigl( f \big|_{k}^{*} (M,\phi) \bigr) (\tau) = \phi(\tau)^{-2k} \rho_{L}^{*}(M,\phi)^{-1} f(M \tau) \]
\end{Def}
With this operator we can define \CLL-valued modular forms:
\begin{Def}
  A holomorphic $f$ is called a \emph{modular form} of weight $k$ with respect to $\rho_{L}^{*}$ and $\mpt{\ZZ}$ if
  \begin{enumerate}[(i)]
  \item $\bigl( f \big|_{k}^{*} (M,\phi) \bigr) = f$ for all $(M,\phi) \in \mpt{\ZZ}$ and
  \item if $f$ is holomorphic at the cusp $\infty$
  \end{enumerate}
where the second condition is explained in the following.
\end{Def}
\begin{Lem}
  If a holomorphic $f$ is invariant under the $\big|_{k}^{*}$-operation of $T$, it has a Fourier expansion
  \begin{equation}
    \label{eq:f_FE}
    f(\tau) = \sum_{\gamma \in L'/L} \sum_{n\in \ZZ - q(\gamma)} c(\gamma,n) \ee_{\gamma}(n\tau)
  \end{equation}
where the Fourier coefficients $c(\gamma,n)$ are calculated by
\begin{equation}
  \label{eq:FEintegral}
   c(\gamma,n) = \int_{0}^{1} \left\langle f(\tau), \ee_{\gamma}(n \bar \tau) \right\rangle \dd x.
\end{equation}
\end{Lem}
\begin{proof}
  Write 
  \begin{equation}
    f = \sum_{\gamma \in L'/L} f_{\gamma} \ee_{\gamma}\label{eq:gamma-comp}
  \end{equation}
  in terms of the $\ee_{\gamma}$ component functions $f_{\gamma}$ of $f$.
  The invariance of $f$ under $T$ means
  \[ \bigl( f \big|_{k}^{*} T \bigr)(\tau) = \sum_{\gamma \in L'/L} f_{\gamma}(\tau + 1) e(q(\gamma)) \ee_{\gamma} = f(\tau). \]
  This means that the screwed functions $\tilde f_{\gamma}(\tau) := e(q(\gamma) \, \tau) f_{\gamma}(\tau)$ are $1$-periodic.
  Plugging in the Fourier expansions for these $\tilde f_{\gamma}$ into \eqref{eq:gamma-comp} yields the result.
\end{proof}
Being holomorphic in the cusp $\infty$ now means that all the $c(\gamma,n)$ in \eqref{eq:f_FE} vanish for $n<0$, so the limit 
\[ \lim_{\tau \rightarrow i \infty} f(\tau) = \sum_{\gamma \in L'/L} c(\gamma,0) \ee_{\gamma} \]
exists, where $c(\gamma, 0) := 0$ if $q(\gamma) \notin \ZZ$.

Let $M_{k,L}$ be the $\CC$-vector space of holomorphic modular forms of weight $k$ with respect to $\rho_{L}^{*}$ and $\mpt{\ZZ}$.
A modular form is called a cusp form if all the $0$-th coefficients vanish, that is, all $c(\gamma,n)$ with $n=0$ vanish. Then $\lim_{\tau \rightarrow i \infty} f(\tau) = 0$. We call the $\CC$-vector space of cusp forms $S_{k,L}$.
The spaces $M_{k,L} \supset S_{k,L}$ are finite dimensional, as shown in \cite[Satz 1.2.26]{Bundschuh2001}.
\begin{Lem}\label{lem:2k-signature-cond}
  It is $M_{k,L} = \{0\}$ if $2k \not\equiv b^{-} - b^{+} \pmod{2}$.
\end{Lem}
\begin{proof} Let $f \in M_{k,L}$.
  Applying \eqref{eq:weilZ} to $Z^{2} = \left( \bigl( \begin{smallmatrix}
   1 & 0  \\ 0 & 1
  \end{smallmatrix} \bigr), -1 \right)$ yields
\[ f(\tau) = \bigl( f \big|_{k}^{*} Z^{2} \bigr)(\tau) = (-1)^{-2k}(-1)^{b^{-} - b^{+}} f(\tau) \]
so $f(\tau) = -f(\tau)$ for $2k \not\equiv b^{-} - b^{+} \pmod{2}$.
\end{proof}

For ease of notation, we write
\[ \epsilon := 2k - b^{-} + b^{+} \]
