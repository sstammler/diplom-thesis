
\section{Results}
\label{sec:results}

We may use the last Proposition~\ref{prop:C-lambda-estimates} together with Corollary~\ref{cor:HeegnerNonTriv} to establish quantitative conditions on the lattice so that the Picard group $\PicQ (Y_{L})$ is non-trivial. The result is
\begin{Thm}\label{thm:result}
  Let $(L,q)$ be an even lattice with the quadratic form $q$ of signature $(2,l)$, $l\geq 3$.
  \begin{enumerate}[(i)]
  \item Suppose $L$ splits a rescaled hyperbolic plane $H(N)$, that
    is, $L=H(N) \perp L_{1} $ for an even lattice $L_{1}$ of signature
    $(1,l-1)$.  If $N \geq N_{1}$ from table~\ref{tab:criticalLevels}, then
    the (negative) Eisenstein coefficient $-C(\lambda,-q(\lambda))$ of
    the Eisenstein series $E=E_{0}$ (cf.~\eqref{eq:eisensteinDef})
    for the test vector $\lambda = \tfrac{1}{N}(1,-1,0) \in L'$ is smaller
    than $s_{\lambda} (l-2)$. Hence the Heegner divisor
    $H(\lambda,q(\lambda))$ defines a non-trivial element of $\PicQ
    (Y_{L})$.

  \item Suppose $L$ splits a hyperbolic plane, $L= H \perp L_{1}$, for an even lattice $L_{1}$ of signature $(1,l-1)$.
    If $N \geq N_{0}$ from table~\ref{tab:criticalLevels}, then
    the (negative) Eisenstein coefficient $-C(\lambda,-q(\lambda))$ of
    the Eisenstein series $E=E_{0}$ of the lattice $L(N)$ for the test vector $\lambda = \tfrac{1}{N}(1,-1,0) \in L(N)'$ is smaller
    than $s_{\lambda} (l-2)$. Hence the Heegner divisor
    $H(\lambda,q(\lambda))$ defines a non-trivial element of $\PicQ
    (Y_{L}(N))$.
  \end{enumerate}
  \begin{table}[!h]
    \centering
    \begin{tabular}{r|ccccccc}
    $l$     & $3$  & $4$ & $5$ & $6$--$8$ & $9$--$10$ & $11$--$26$ & $l\geq 27$ \\
    \hline
    $N_{0}$ & $4$  & $4$ & $3$ &   $2$    &   $2$     &     $2$     &   $1$ \\
    $N_{1}$ & $10$ & $6$ & $4$ &   $4$    &   $3$     &     $2$     &   $1$
  \end{tabular}
\caption{The smallest levels for which the Picard group is guaranteed to be non-trivial}\label{tab:criticalLevels}
\end{table}
\end{Thm}
\begin{proof}We will first argue, why we only have to calculate the bounds~\eqref{eqn:C-lambda-estimates} for finitely many values of $l$ and $N$.
  The zeta functions (which quickly converge to $1$ for increasing $k$) and the constants $1/\sqrt{2}^{\delta_{2}}$ and $2$ from the bounds of Proposition~\ref{prop:C-lambda-estimates} are clearly bounded by a constant for increasing $k\geq5/2$ (corresponding to $l\geq 3$). Also, the bounds are strictly monotonic decreasing for increasing $N$ (of a given parity, mind $K_{N}$). Using the Stirling approximation 
  \[ \Gamma(k+1) \sim \sqrt{2 \pi k} \left( \frac{k}{e} \right)^{k} \]
for the $\Gamma$-function, we can give a (weak) upper bound of
\begin{equation*}
  \frac{C}{2^{k}} \text{ for some constant $C \geq 0$ and $k\geq 18$,}
\end{equation*}
noting that $2 \pi e < 18$. Further note that the bounds~\eqref{eqn:C-lambda-estimates} will clearly decrease strictly monotonic for $k\geq 18$ increasing for a given parity of $N$. (In fact, they already do so for smaller $k$ because of the strong growth of $\Gamma$.) So it suffices to check only finitely many combinations of $l$ and $N$. That can be done by a computer program. I used the Mathematics software \emph{Sage}, one can find the source code in Appendix~\ref{cha:sage-sourcecode}.
Note that $s_{\lambda} = 2$ for $N\leq 2$ and $s_{\lambda}=1$ otherwise.

Corollary~\ref{cor:HeegnerNonTriv} now tells us, that if the computed bounds fall below $s_{\lambda}(l-2)$, then the Heegner divisor $H(\lambda,q(\lambda))$ defines a non-trivial element in the Picard group $\PicQ (Y_{L})$, resp.\ $\PicQ (Y_{L}(N))$.
\end{proof}

\subsection{Discussion}
\label{sec:discussion}

Without taking more assumptions on the lattice, I think that the result of the last theorem is not too far from the best bounds possible. For $l=26$ the unimodular lattice $I\!I_{2,26}$ of signature $(2,26)$ has indeed a trivial Picard group $\PicQ (X_{I\!I_{2,26}})$. This follows from a result of Borcherds on automorphic products, which implies that every Heegner divisor $H(\beta,n)$ is trivial, cf.~\cite[Thm.~13.3, \pno~48]{Borcherds1998}. Hence the table~\ref{tab:criticalLevels} is optimal at the right side.

The estimates in section~\ref{sec:upper-bounds-N} were conducted quite carefully, so I don't think that there is much room for improvement. Also note, that if $N | \det S$, but we don't know about any other primes dividing the determinant, then it cannot be guaranteed, that there is a vector $\lambda \in L'$ for which $n \in \ZZ - q(\lambda)$ can be smaller than $1/N$. So the test vector chosen here seems to me to be the best choice in this general setting.

The crudest estimates happened in the proof of Proposition~\ref{prop:C-lambda-estimates} while estimating \eqref{eqn:20}, leading to the product~\eqref{eqn:crudeEstimate} over the primes $p$ dividing $\det L_{1}$ but not $2 N$. If one knows more about the prime factorization of $\det S$, here is the biggest room for improvement. So in given particular cases (i.e., if we actually know the lattice), this estimate could be enhanced and one would get a lower limit on $N$ for this particular lattice.

But then again, the product could be empty, not taking more assumptions on $L_{1}$, so in general we cannot give a better bound for this product than $1$.