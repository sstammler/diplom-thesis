
\section{A reasonable test vector}
\label{sec:test-vector}

As mentioned in the introduction, we will take some assumptions on the lattice $L$. In the following assume, that $L$ splits a rescaled hyperbolic plane,
\[ L = H(N) \perp L_{1} \]
with $L_{1}$ an even Lorentzian lattice and $H(N)$ as in section~\ref{sec:hyperbolic-planes} for some $N \in \NN$. In view of Corollary~\ref{cor:split2hypPlanes} is the assumption not very restrictive. Let $\{ f_{1}, f_{2} \}$ be a hyperbolic basis for $H(N)$, cf.\ Definition~\ref{def:hyperbolicPlane}. Notice that $(f_{i},\xx)=0$, $i=1,2$, for all $\xx \in L_{1}$. Write $(x_{1},x_{2},\xx) = x_{1}f_{1} + x_{2} f_{2} + \xx$ for the decomposition of a vector $(x_{1},x_{2},\xx) \in V_{\QQ} = L \otimes \QQ = \QQ^{2} \perp L_{1} \otimes \QQ$. 

We want to define a good test vector $\lambda \in L'$. Recall our formula for $C(\gamma,n)$ from Theorem~\ref{thm:C-gamma-n-k-mHALF}. In order to minimize $C(\lambda,n)$, all terms involving $n$ and $\gamma = \lambda$ have to be minimized. The first factor of the formula involves the term $n^{k-1}$, which is the strongest contributor for large $k$, so it seems reasonable to try to get $n \in \ZZ - q(\lambda)$  as small as possible.

We don't make any assumptions on the structure of $L_{1}$, so it is reasonable to make the ansatz $\lambda \in H(N)' \perp 0$ because we cannot evaluate and thus minimize terms involving $q((x_{1}/N, x_{2}/N, \xx))$ if $\xx \neq 0$. Notice that $\frac 1 N (x_{1},x_{2})$, $x_{i} \in \ZZ$, is the most general form for a vector in $H(N)'$.

Now 
\[ q(\lambda) = \frac{1}{N^{2}} q(x_{1} f_{1} + x_{2} f_{2}) =
    \frac{x_{1} x_{2}}{N}. \]
If we choose $x_{1}=1$ and $x_{2}=-1$, it is $q(\lambda) = -1/N$ and $n \in \ZZ - q(\lambda)$ can be chosen to be $n = 1/N$. Thus we seek upper bounds for $C(\lambda, -q(\lambda))$, which has already been mentioned in the previous chapter.

The formula~\eqref{eq:C-gamma-n-k-mHALF} evaluates to:
\begin{Prop}\label{prop:C-lambda_1}
  the Eisenstein coefficient $C(\lambda, -q(\lambda))$ for the test vector $\lambda$ is
  \begin{multline}
    C(\lambda, -q(\lambda)) = -\frac{2^{k+1} \pi^{k}} {N^{k-1}\sqrt{|L'/L|}\Gamma(k)}  \\
      \qquad \times \left\{ 
    \begin{IEEEeqnarraybox}[\relax][c]{rCl'u}
        \frac{2^{3(1-2k)} N_{\lambda}(2^{3})}{L(k,\chi_{4D})}
        & \smashoperator{\prod_{\substack{p| \det S \\ p \neq 2}}} &
          p^{1-2k} N_{\lambda}(p), &
           if $l$ is even, \\
        \frac{L(k- \tfrac{1}{2}, \chi_{4 \DD})}{\zeta(2k-1)} \frac{2^{3(1-2k)} N_{\lambda}(2^{3})}{1 - 2^{1-2k}} 
        & \smashoperator{\prod_{\substack{p| \det S \\ p \neq 2}}} &
        \frac{p^{1-2k} N_{\lambda}(p)}{1-p^{1-2k}}, & if $l$ is odd.
    \end{IEEEeqnarraybox} \right. \label{eqn:C-lambda-exact}
  \end{multline}
 We wrote $N_{\lambda} := N_{\lambda, -q(\lambda)}$ and as usual denoted by $S$ the Gram matrix for the lattice $(L,q)$. The quantities $D, \DD$ are just as in Theorem~\ref{thm:C-gamma-n-k-mHALF}.
\end{Prop}
\begin{proof}
  Recall formula~\eqref{eq:C-gamma-n-k-mHALF}.
  Obviously $d_{\lambda} = N$. Recall the definition of $w_{p}  = 1+ 2v_{p}(2nd_{\lambda}) = 1 + 2v_{p}(2)$, hence
\[ w_{p} =
\begin{cases}
  3 & \text{if $p=2$,} \\
  1 & \text{otherwise.}
\end{cases}
\]
With the notation of Lemma~\ref{lemma:Siegel2} it is $f=1$ as $n=1/N$ and $\tilde n = n d_{\lambda}^{2} = N | \det S | D, \DD$. So the term $\sigma_{1-k}(\tilde n, \chi_{4D})$ is simply $1$ and the sum $\sum_{d | f}$ for odd $l$ trivially evaluates to $1$.

Or even easier, this is just formula~\eqref{eq:10} from the proof of Theorem~\ref{thm:C-gamma-n-k-mHALF}, using that $\tilde n | \det S$. Also because of this, the characters $\chi_{D}$, $\chi_{\DD}$ inside the product vanish for all primes $p | \det S | D,\DD$.

Now take the factor corresponding to $p=2$ out of the product over the primes $p$ dividing $2 \det S$.
\end{proof}
