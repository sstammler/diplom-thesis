\section{Modular forms}
\label{sec:modular-forms}

We will now define modular forms on the Hermitian symmetric spaces. For the general case of rational weights, one needs multiplier systems $\chi$ for the groups $\Gamma \subset \Gamma_{L}$ and automorphy factors $j: O'(V) \times \HH_{l}' \rightarrow \CC$ for $O'(V)$ satisfying cocycle relations. We don't want to define automorphy factors here, one can find a good introduction in \cite[\ppno~84]{Bruinier2002}. The general definition is also necessary to define cusp forms.

\subsection{Rational weight}
\label{sec:rational-weight}

Fix a branch of the usual logarithm $\Log$ on $\CC$. For $k \in \QQ$ we write
\begin{equation*}
  j(\sigma,z)^{k} := e^{k \Log j(\sigma,z)}
\end{equation*}
for the $k$-th power of $j$. There is a map $w_{k}$ mapping $O'(V) \times  O'(V)$ into the roots of unity that \emph{fixes} the cocycle relation for the automorphy factor in the $k$-th power,
\begin{equation*}
  j(\sigma_{1} \sigma_{2}, z)^{k} = w_{k}(\sigma_{1},\sigma_{2}) j(\sigma_{1},\sigma_{2}z)^{k} j(\sigma_{2},z)^{k}.
\end{equation*}
The map $w_{k}$ only depends on $k$ modulo $\ZZ$ and its order is bounded by the denominator of $k$. At least we can now define multiplier systems.
\begin{Def}
  Let $\Gamma \subset O'(V)$ be a subgroup and $k \in \QQ$. A \emph{multiplier system} $\chi$ of weight $k$ for $\Gamma$ is a map
  \[ \chi: \Gamma \longrightarrow S^{1} \]
into the unit circle that satisfies
\[ \chi(\sigma_{1} \sigma_{2}) = w_{k}(\sigma_{1},\sigma_{2}) \chi(\sigma_{1}) \chi(\sigma_{2}). \]
The product $\chi(\sigma) j(\sigma,z)^{k}$ is a cocycle of $\Gamma$.
Obviously, if $k$ is integral, then $\chi$ is just an ordinary character.
\end{Def}
The general definition of modular forms now looks as follows.
\begin{Def}\label{def:modular-forms-rational-w}
  Let $k \in \QQ$, $\Gamma \subset \Gamma_{L}$ be a finite index subgroup and $\chi$ a multiplier system of weight $k$ for $\Gamma$. A meromorphic (resp.\ holomorphic) function $F$ on $\HH_{l}'$ is called a \emph{meromorphic (resp.\ holomorphic) modular form} of weight $k$ and multiplier system $\chi$ with respect to $\Gamma$, if it satisfies
  \begin{equation*}
    F(\sigma z) = \chi(\sigma) j(\sigma,z)^{k} F(z)
  \end{equation*}
  for all $\sigma \in \Gamma$.
\end{Def}

There is a lattice $\Lambda \subset W_{\QQ}$, coming from special so called Eichler transformations, for which holomorphic modular forms to the trivial character are periodic, see \cite[38]{FreitagOrthogonalGr} or \cite[86]{Bruinier2002}. This yields a Fourier expansion
\begin{equation*}
  F(z) = \sum_{\lambda \in \Lambda'} a(\lambda) e((\lambda,z)),
\end{equation*}
with $\Lambda'$ being the dual lattice of $\Lambda$ with respect to the bilinear form of $V$ restricted to $W$.
\begin{Def}\label{def:entire_cusp}
  \begin{enumerate}[(i)]
  \item A modular form is called \emph{entire at infinity} if
    \[ a(\lambda) \neq 0 \text{ implies } \lambda \in \bar \calP^{+} \subset \bar \calP \]
    where $\bar \calP^{+}$ is the closure of $\calP^{+}$.
  \item Such a modular form is called a \emph{cusp form} if even
    \[ a(\lambda) \neq 0 \text{ implies } \lambda \in \calP^{+}. \]
\end{enumerate}
\end{Def}
The Koecher principle \cite[Prop.~4.15,\pno~109]{Bruinier2002} now says that in our case $l \geq 3$ all modular forms are entire at infinity. The definition of a cusp form now seems analogous to the classical notion if one considers $\QQ \cup \{ \infty \}$ as the boundary of $\HH$.

\subsection{Integer weight}
\label{sec:integer-weight}

It sometimes comes in handy to work with modular forms on $\HH_{l} \subset \calK$ instead. Unfortunately it is not possible to allow non-integer weights in this case, but the transformation under $\Gamma$ doesn't involve the automorphy factors. We actually work with the cone over $\HH_{l} \subset P(V(\CC))$, it is defined by
\[ \widetilde \HH_{l} = \setb{Z \in V(\CC) \setminus \{0\} }{ [Z] \in \HH_{l}}. \]

\begin{Def}\label{def:modular-forms-integer-w}
  Let $k \in \ZZ$, $\Gamma \subset \Gamma_{L}$ a finite index subgroup and $\chi$ a character of $\Gamma$. A meromorphic (resp.\ holomorphic) function $F$ on $\widetilde \HH_{l}$ is called a meromorphic (resp. holomorphic) modular form of weight $k$ and character $\chi$ with respect to $\Gamma$ if
  \begin{enumerate}[(i)]
    \item $F$ is homogenous of degree $-k$, i.e.,
      \[ F(cZ) = c^{-k} F(Z) \text{ for all } c \in \CC \setminus \{0\}, \]
    \item $F$ is invariant under $\Gamma$ in the sense that
      \[ F(\sigma Z) = \chi(\sigma) F(Z) \text{ for all } \sigma \in \Gamma, \]
    \item $F$ is meromorphic (resp.\ holomorphic) at the Shitake boundary.\label{item:MFDefBoundary}
  \end{enumerate}
\end{Def}
The \emph{Witt rank} of $L$ is the dimension of a maximal isotropic subspace of $L \otimes \QQ = V_{\QQ}$. Because the signature is $(2,l)$, the Witt rank is at most $2$. Because $l \geq 3$, the Koecher principle ensures that condition \eqref{item:MFDefBoundary} is automatically fulfilled.

If $\Gamma$ acts freely on $\HH_{l}$ and $Y(\Gamma)$ is non-singular, then meromorphic (resp.\ holomorphic) modular forms of weight $k$ with fixed multiplier system $\chi$ for $\Gamma$ can be viewed as global rational (resp. regular) sections of an algebraic line bundle on $Y(\Gamma)$.
\begin{Def}
  Let $\calM_{k}(\Gamma,\chi)$ = $\calM_{k}(\chi)$ be the line bundle of modular forms of weight $k$ for the multiplier system $\chi$ with respect to $\Gamma$ on $Y(\Gamma)$. Abbreviate $\calM_{k}=\calM_{k}(1)$ for the trivial character. Furthermore let $\calS_{k}(\Gamma,\chi) \subset \calM_{k}(\Gamma,\chi)$ denote the line bundle of holomorphic cusp forms in $\calM_{k}(\Gamma,\chi)$.
\end{Def}
If $k \in \ZZ$, then we have $\calM_{k} \cong \calM_{1}^{\otimes k}$. Modular forms of negative weight vanish identically and those of zero weight are constant. We now state an important result concerning possible weights of modular forms.
\begin{Prop}[Singular weights]\label{prop:singular-weights}
  Let $L$ be an even lattice (of signature $(2,l)$) that splits two hyperbolic planes over $\QQ$, cf.\ Lemma~\ref{lem:split-hypPl}. If $F$ is a modular form of weight $k\in\QQ$ for some multiplier system of finite order, then
\[ k \geq \frac{l}{2} - 1 \]
\end{Prop}
For a proof, consult \cite[Satz~3.1.19,~\pno~60]{Bundschuh2001}.
