
\section{Upper bounds}
\label{sec:upper-bounds-calc}

In order to get universal bounds for $C(\lambda, -q(\lambda))$ independent of the structure of the unknown lattice $L_{1}$, we have to estimate the dependencies on $L_{1}$, resp.\ $L$. The discriminants $D$ and $\DD$ are divisible by $N$, but we don't know which other primes divide them. But we have the estimates
\begin{equation}
  \label{eqn:EulerProdEsti}
  \prod_{p \text{ prime}} \frac{1}{1+ p^{-k}} = \frac{\zeta(2k)}{\zeta(k)} \leq L(k,\chi) \leq \zeta(k)
\end{equation}
for any $L$-function $L(k,\chi)$ to a Dirichlet character $\chi$, or in fact for any function
$\chi: \NN \rightarrow \{0, \pm 1 \}$. This gives us
\begin{Lem}\label{lem:L-estimate}
  The contribution of Dirichlet $L$-series terms in Proposition~\ref{prop:C-lambda_1} can be bounded from above by
  \begin{IEEEeqnarray*}{rCLs}
    \frac{1}{L(k,\chi_{4D})} & \leq & \frac{\zeta(k)}{\zeta(2k)} & and \\
    \frac{L(k - \tfrac{1}{2}, \chi_{4 \DD})}{\zeta(2k -1)} \prod_{p | 2 \det S} \frac{1}{1 - p^{1-2k}} & \leq & \frac{\zeta(k- \tfrac{1}{2}) \zeta(2k -1)}{\zeta(4k -2)}.    
  \end{IEEEeqnarray*}
\end{Lem}
\begin{proof}
The first estimate is just \eqref{eqn:EulerProdEsti}. For the second one, notice that the factors $(1-p^{1-2k})^{-1}$ cancel out the factors in the Euler product of $\zeta(2k -1)^{-1}$ for the primes $p$ dividing $2 \det S$. So we have an $L$-function on the left side, for the character $\chi_{(2 \det S)^{2}}$ and argument $2k-1$. We wouldn't even need it do be a real character, see the remark after estimate~\eqref{eqn:EulerProdEsti}.
\end{proof}

\subsection{Upper bounds for the representation number \texorpdfstring{$N_{\gamma}$}{}}
\label{sec:upper-bounds-N}
We have eliminated the dependencies on the discriminant. It remains to find estimates for the representation numbers $N_{\lambda}$ universally valid for all sub lattices $L_{1}$. We shall start by rewriting the congruence relation.
\begin{Lem}
  Using the decomposition $L = H(N) \perp L_{1}$, we can restate Definition~\eqref{eq:N_gamma-a} as
  \begin{multline}
    \label{eqn:N-lambda-hypPl}
    N_{\lambda} (p^{\nu}) \\
      = \# \setb{(x_{1}, x_{2}, \xx) \in (\ZZ / p^{\nu} \ZZ)^{2} \perp L_{1}/ p^{\nu} L_{1}}{(Nx_{2} + 1) x_{1} - x_{2} + q(\xx) \equiv 0 \pmod{p^{\nu}}}
  \end{multline}
\end{Lem}
\begin{proof}
  This is a simple calculation,
  \begin{IEEEeqnarray*}{+rCl+x*}
    q(x - \lambda) + n & = & q((x_{1},x_{2}, \xx)) - ((x_{1},x_{2},\xx),\lambda) \\
      & = & x_{1} x_{2} N + q(\xx) - (x_{2} - x_{1}). & \qedhere
  \end{IEEEeqnarray*}
\end{proof}
\begin{Lem}\label{lem:N-lambda_p-DIV-N}
Let $p$ be a prime dividing $N$. Then
\begin{equation}\label{eqn:N-lambda_p-DIV-N}
  p^{\nu (1-2k)} N_{\lambda} (p^{\nu}) = 1
\end{equation}  
\end{Lem}
\begin{proof}
  Notice that $l+1 = 2k-1$. There are $p^{\nu (2k-1)}$ possibilities to choose $(x_{2},\xx) \in \ZZ / p^{\nu} \ZZ \perp L_{1} / p^{\nu} L_{1}$. Fix such a choice. Since $p | N$, the coefficient $N x_{2} +1$ of $x_{1}$ in the congruence~\eqref{eqn:N-lambda-hypPl} is always prime to $p$. Hence the linear congruence modulo $p^{\nu}$ has exactly one solution for $x_{1}$.
\end{proof}
\begin{Lem}\label{lem:N-lambda_p-DIV-detS}
  Let $p$ be an odd prime dividing $\det S$, but not $N$. Then
  \begin{equation}
    \label{eqn:N-lambda_p-DIV-detS}
    p^{1-2k} N_{\lambda} (p) \leq 1+ \frac{1}{p}    
  \end{equation}
\end{Lem}
\begin{proof}
  we have to solve the congruence~\eqref{eqn:N-lambda-hypPl} over the field $\ZZ / p \ZZ = \FFp$. The map $x_{2} \mapsto N x_{2} +1$ is bijective, as $p \notdivides N$. So there is one $x_{2}=x_{2}^{*}$ which gets mapped to $0$. For the remaining $p-1$ choices of $x_{2}$, the coefficient $N x_{2} +1$ of $x_{1}$ is a unit, and thus the congruence has exactly one solution for $x_{1}$, for every of the $p^{2k-2}$ choices of $\xx \in L_{1}/pL_{1}$.

Now we analyze the solution count for $x_{2} = x_{2}^{*}$. The congruence reads now
\begin{equation}
  \label{eqn:21}
  q(\xx) \equiv x_{2}^{*} \pmod p
\end{equation}
because the coefficient of $x_{1}$ is $0$. For all solutions $\xx$ of this congruence, every $x_{1}$ is a solution, and for the others, none. We count the solutions $\xx$ of this congruence. Write again $\xx$ for a coordinate vector $\xx \in \FFp^{l}$ which represents $\xx \in L_{1}/p L_{1}$ for any lattice basis, and let $S$ be the Gram matrix of $q$, cf.\ \eqref{eq:GramQuadrForm}. If $q$ is trivial over $\FFp^{l}$, there are no solutions at all, since $x_{2}^{*}$ is non-zero. So w.l.o.g.\ we assume, that $q$ is non-degenerate on $\FFp^{l}$ (otherwise just consider $q$ on a orthogonal subspace of $\FFp^{l}$ on which $q$ is non-degenerate). Since $\FFp$ is a field, we may diagonalize the symmetric non-degenerate Gram matrix (e.g., using the Gram-Schmidt process), to obtain a new lattice basis for which the Gram matrix is diagonal, which we assume for $S$ now. If we fix $p^{l-1}$ coordinates $y_{2}, \ldots, y_{l} \in \FFp$, the congruence becomes
\[ \tfrac{S_{11}}{2} y_{1}^{2} + q((0,y_{2},\ldots,y_{l})) - x_{2}^{*} \equiv 0 \pmod p \]
which is a quadratic congruence in $y_{1}$ with non-zero coefficient and thus has at most $2$ solutions.
So, for $x_{2} = x_{2}^{*}$, there are at most $2 p^{l-1} p = 2 p^{2k-2}$ solutions of the congruence~\eqref{eqn:21} for $(x_{1},\xx)$.

In total, we showed, that
\[ p^{1-2k} N_{\lambda}(p) \leq p^{1-2k} p^{2k-2}((p-1) +2) = 1 + \frac{1}{p}. \qedhere\]
\end{proof}
It remains to inspect the quantity $2^{3 (1-2k)} N_{\lambda}(2^{3})$. For even $N$ Lemma~\ref{lem:N-lambda_p-DIV-N} tells us that it equals $1$. For odd $N$ we first show the following
\begin{Lem}\label{lem:mod8Congruence}
  Let $L$ be an even non-degenerate lattice of rank $l$ with the quadratic form $q$ and let $\varepsilon \in (\ZZ/ 8 \ZZ)^{*}$ be a unit. Then the congruence
  \begin{equation}
    \label{eqn:mod8Congr}
    q(x) \equiv \varepsilon \pmod 8
  \end{equation}
has at most $8^{l}/2$ solutions for $x \in L/ 8L$.
\end{Lem}
\begin{proof}
  We use the theory of $p$-adic classification from section~\ref{sec:class-quadr-forms} for $p=2$. Let
  \[ L \sim_{2} 2^{0} L_{0} \perp 2^{1} L_{1} \perp \ldots \]
  be the $2$-adic decomposition of $L$. If $L_{0}$ is $0$, then the congruence~\eqref{eqn:mod8Congr} over $\ZZ_{2} / 2^{3} \ZZ_{2} = \ZZ/ 2^{3} \ZZ$ has no solutions as $q$ would only take even values.
So suppose $L_{0}$ is non-trivial. If its Gram matrix contains a $1 \otimes 1$ block $(b)$,  $b \in (\ZZ/ 8\ZZ)^{*}$ on its diagonal, let it w.l.o.g.\ be the first entry on the diagonal. Fixing the last $l-1$ coordinates of a vector $x \in (\ZZ/ 8\ZZ)^{l}$, the congruence~\eqref{eqn:mod8Congr} becomes the quadratic congruence (after multiplying with $b^{-1}$)
\[ x_{1} \equiv c \pmod 8  \]
for some unknown constant $c(=b^{-1}(\varepsilon-q((0,x_{2},\ldots,x_{l})))$. In the worst case that $c=1$, this congruence has the four units of $\ZZ/ 8\ZZ$ as solution. This would yield the upper bound $8^{l-1}\cdot 4 = 8^{l}/2$ as stated in the lemma.

In the other two cases, that the Gram matrix of $L_{0}$ only contains the matrices~\eqref{eqn:2-adicMatrices} on its diagonal, there are even less solutions. This can be shown by some tedious calculations by cases. Fix the last $l-2$ coordinates of $x \in (\ZZ/ 8\ZZ)^{l}$. The congruence becomes now one of the two cases
\begin{IEEEeqnarray}{rClu}
  2 x_{1} x_{2} & \equiv & c \pmod 8, & or \IEEEyessubnumber \label{eqn:Mod8_case1} \\
  2 ({x_{1}}^{2} + x_{1} x_{2} + {x_{2}}^{2}) & \equiv & c \pmod 8  \IEEEyessubnumber \label{eqn:Mod8_case2}
\end{IEEEeqnarray}
again for some unknown constant $c$.
Instead of doing the tedious calculations, one can also just issue the script
\begin{minted}{python}
for M in [[0,1,0],[1,1,1]]:
     H = QuadraticForm(ZZ,2,M)
     for c in range(8):
         H.count_congruence_solutions(2,3,c,None,None)
\end{minted}
in Sage, to find that the congruence~\eqref{eqn:Mod8_case1} has at most $20 < 8^{2}/2$ solutions and the second congruence~\eqref{eqn:Mod8_case2} has at most $12 < 8^{2}/2$ solutions.
\end{proof}
Thanks to this lemma, we can give the following estimate for the remaining $N_{\lambda}(2^{3})$-term:
\begin{Lem}\label{lem:N-lambda-8}
  If $N$ is odd, then
  \begin{equation}
    \label{eqn:N-lambda-8}
    2^{3 (1-2k)} N_{\lambda}(2^{3}) \leq 2.
  \end{equation}
\end{Lem}
\begin{proof}
  Again, we have to solve the congruence~\eqref{eqn:N-lambda-hypPl} over $\ZZ / 8 \ZZ$. Define $\alpha(x_{2}) := \min \{ v_{2}(N x_{2}+1),3 \}$ and solving the congruence means to solve
\[ 2^{\alpha (x_{2})} x_{1} - x_{2} + q(\xx) \equiv 0 \pmod 8 .\]
Note that as in the proof of Lemma~\ref{lem:N-lambda_p-DIV-detS} $x_{2} \mapsto N x_{2} + 1$ is a bijection, which is crucial for the counting argument. Due to Hensel's Lemma, this congruence has at most $2^{\alpha(x_{2})}$ solutions for fixed $x_{2}$ and $\xx$. We use this argument for the seven values of $x_{2}$ for which $N x_{2} +1$ is non-zero, i.e., where $\alpha(x_{2}) \neq 3$. Hence, for this seven $x_{2}$, the congruence has at most
\begin{IEEEeqnarray*}{Cl}
  & 2^{3l} \sum_{i=0}^{2} \# \{x_{2} | \, \alpha(x_{2}) = i \}\cdot 2^{i} \\
       = & 8^{l} (4 \cdot 2^{0} + 2 \cdot 2^{1} + 1 \cdot 2^{2} ) = 8^{l} \cdot 12
\end{IEEEeqnarray*}
solutions. In the case $\alpha(x_{2})=3$ this congruence is exactly the congruence from the previous lemma, which adds another $8 \cdot 8^{l}/2$ solutions, where the additional factor $8$ comes from the $x_{1}$, which can now be arbitrarily chosen.

Thus in total
\[ 2^{3(1-2k)} N_{\lambda}(2^{3}) \leq 8^{-l-1+l} \cdot (12 + 4) = 2 \qedhere \]
\end{proof}

\subsection{The Eisenstein coefficient}
\label{sec:eisenst-coeff}

We put the estimates of Lemmas~\ref{lem:L-estimate} to~\ref{lem:N-lambda-8} into formula~\eqref{eqn:C-lambda-exact} for the Eisenstein coefficient $C(\lambda, -q(\lambda))$ to conclude
\begin{Prop}\label{prop:C-lambda-estimates}
  the Eisenstein coefficients $C(\lambda, -q(\lambda))$ for the test vector $\lambda \in L'$ are bounded by
  \begin{equation}
    \label{eqn:C-lambda-estimates}
    - C(\lambda, -q(\lambda)) \leq \frac{2^{k+1} \pi^{k} K_{N}}{N^{k_{L}} \Gamma(k)} 
      \times
      \begin{dcases}
        \frac{\zeta(k)}{\zeta(2k)} & \text{for $l$ even,} \\
        \frac{\zeta(k- \tfrac{1}{2}) \zeta(2k -1)}{\sqrt{2}^{\delta_{2}}\zeta(4k -2)} & \text{for $l$ odd,}
      \end{dcases}
  \end{equation}
  with the constants
\[ K_{N} :=
\begin{cases}
  1 & \text{if $N$ is even,} \\
  2 & \text{if $N$ is odd,}
\end{cases} \]
and $k_{L}$, $\delta_{2}$ as follows. If the sub lattice $L_{1}$ is also rescaled by $N$, i.e., $L_{1} = L_{2}(N)$ for some even Lorentzian lattice $L_{2}$, and thus $L = (H \perp L_{2})(N)$, then we may choose $k_{L} = 2k -1$. If we don't have this information, we can only choose $k_{L} = k$, which yields a weaker bound. The exponent $\delta_{2}$ shall be $0$ in the one case that $L_{1}$ is rescaled by $N=2$, and $1$ otherwise.
\end{Prop}
\begin{proof}
  We insert the estimates of the Lemmas~\ref{lem:L-estimate} to~\ref{lem:N-lambda-8} into \eqref{eqn:C-lambda-exact} and see, that the coefficient $-C(\lambda, -q(\lambda))$ is bounded by
  \begin{equation*}
    \frac{2^{k+1} \pi^{k} K_{N}} {N^{k-1}\sqrt{|L'/L|}\Gamma(k)} \prod_{\substack{p | \det S \\ p \notdivides 2N}} \left( 1 + \frac{1}{p} \right) \times 
    \begin{dcases}
        \frac{\zeta(k)}{\zeta(2k)} & \text{for $l$ even,} \\
        \frac{\zeta(k- \tfrac{1}{2}) \zeta(2k -1)}{\zeta(4k -2)} & \text{for $l$ odd.}
      \end{dcases}
  \end{equation*}
We have to analyze the term
\begin{equation}
\frac{1}{\sqrt{\det S}} \prod_{\substack{p | \det S \\ p \notdivides 2N}} \left( 1+ \frac{1}{p} \right).\label{eqn:20}
\end{equation}

First observe that we can write $\sqrt{\det L} = N \sqrt{\det L_{1}}$ taking the determinant $N^{2}$ of $H(N)$ out of the square root.

If $L_1$ rescales by $N$, we can take an additional factor of $N^{l/2} = N^{k-1}$ out of $\sqrt{\det L_{1}}$. If $l$ is odd and $N$ was not $2$, we can take another factor of $\sqrt{2}$ out of $\sqrt{\det L_{1}}$ as $L_{1}$ is an even lattice of odd dimension $l$, cf.\ Lemma~\ref{lem:evenDet-oddDim}. This gives us the exponent $\delta_{2}$ described in the theorem.

In any case, what remains is bounded from above by
\begin{equation}
 \prod_{\substack{p | \det L_{1} \\ p \notdivides 2N}} \frac{1}{\sqrt{p}} \left( 1 + \frac{1}{p} \right)\label{eqn:crudeEstimate}
\end{equation}

because we don't know the multiplicity of the prime factors in $\det L_{1}$. For every prime $p \geq 3$, the factors are already smaller than $1$, so the product is bounded by $1$.

Summed up, \eqref{eqn:20} is bounded by $1/N$, resp.\ $1/N^{k}$ if $L_{1}$ rescales by $N$, and if additionally $l$ is odd and $L_{1}$ doesn't rescale by $2$, the upper bound gains the extra factor $1/\sqrt{2}$.
\end{proof}
