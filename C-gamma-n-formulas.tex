\subsection[Formulas for the Eisenstein coefficients]{Formulas for the Eisenstein coefficients \texorpdfstring{$C(\gamma,n)$}{C(ɣ,n)}}
\label{sec:C-gamma-n-formulas}
We will now state the main results of this chapter, two computable formulas for the Eisenstein coefficients.
The first theorem gives a formula for general weights $k>2$ whereas the second theorem treats the special but important case $k= \frac{m}{2}$.
The formulas are mainly a result of Lemma~\ref{lemma:Siegel2} from the preceding section~\ref{sec:calcs-N_gamma_n}.

\begin{Thm}
  \label{thm:C-gamma-n-general-k}
  Let $\gamma \in L'$ and $n \in \ZZ - q(\gamma), n > 0$. The discriminants $D, \DD$ are defined as in Lemma~\ref{lemma:Siegel2}. 
Then the Fourier coefficient $C(\gamma,n)$ equals 
    \begin{multline}
      C(\gamma,n) = \frac{2^{k+1} \pi^{k} n^{k-1} (-1)^{\epsilon/4}}{\sqrt{|L'/L|}\Gamma(k)}  \\
      \qquad
      \times \begin{dcases}
        \frac{1}{L(k,\chi_{D})} \prod_{p| 2 \tilde n \det S} \frac{L_{\gamma,n}(k,p)}{1-\chi_{D}(p)p^{-k}}, &
           \text{if $m$ is even,} \\
        \frac{L(k- \tfrac{1}{2}, \chi_{\DD})}{\zeta(2k-1)} \ \prod_{\mathclap{p|2 \tilde n \det S}} \
            \frac{1-\chi_{\DD}(p)p^{1/2-k}}{1-p^{1-2k}} L_{\gamma,n}(k,p), & \text{if $m$ is odd}
      \end{dcases}\label{eq:C-gamma-n-general-k}
    \end{multline}
with the local Euler factors $L_{\gamma,n}(k,p)$ given by \eqref{eq:Lgamma-n-p}.
\end{Thm}
\begin{proof}
  Inserting \eqref{eq:Lgamma-n-EPlocal} into Proposition~\ref{prop:C-L-series} yields
  \begin{equation}
    \label{eq:C-prod-local-L}
    C(\gamma,n) = \frac{2^{k+1} \pi^{k} n^{k-1} (-1)^{\epsilon/4}}{\sqrt{|L'/L|} \Gamma(k)}
        \prod_{p \text{ prime}} L_{\gamma,n}(k,p).
  \end{equation}
Let $p$ be a prime coprime to $2 \tilde n \det S$.
Applying Lemma~\ref{lemma:Siegel2} with $\alpha = 1$ (it is $v_{p}(n) = 0$ here) gives us
\begin{equation}
  \label{eq:3}
  p^{1-m} N_{\gamma,n}(p) = 1 - \chi_{D}(p) p^{-m/2} \quad \text{for even $m$.}
\end{equation}
For odd $m$ note that $w_{p}=1$ and  $v_{p}(f) = 0$ as $p$ and $\tilde n = \tilde n_{0} f^{2}$ are coprime; we get
\begin{IEEEeqnarray}{rCl}
  p^{1-m} N_{\gamma,n}(p) & = & \frac{1-p^{1-m}}{1-\chi_{\DD}(p)p^{(1-m)/2}} \nonumber \\
    &=& \frac{1-p^{1-m} }{\big( 1 - \underbrace{\chi_{\DD}(p)^{2}}_{=1\: (\star)} p^{(1-m)/2} \big)}
        \big( 1 + \chi_{\DD}(p) p^{(1-m)/2} \big) \nonumber \\
    &=& \big( 1 + \chi_{\DD}(p) p^{(1-m)/2} \big) \label{eq:7}
\end{IEEEeqnarray}
where we used at $(\star)$ that $(\DD,p)=1$.

We use this to calculate the local Euler factors, noting again that $w_{p}=1$.
For even $m$ the local Euler factor \eqref{eq:Lgamma-n-p} becomes
\begin{IEEEeqnarray}{rCl}
  L_{\gamma,n}(s,p) & = & 1- p^{m/2-s} + N_{\gamma,n}(p) p^{1-m/2-s} \nonumber \\
    &=& 1 - \chi_{D} (p) p^{-s} \quad \text{for even $m$,}\label{eq:8}
\end{IEEEeqnarray}
where the second equality follows after inserting \eqref{eq:3}.
And for odd $m$ we insert \eqref{eq:7} and get
\begin{eqnarray}
  L_{\gamma,n}(s,p) & = & 1 - p^{m/2-s} + \big( 1 + \chi_{\DD}(p) p^{(1/m)/2} \big) p^{m/2-s} \nonumber \\
    &=& 1+ \chi_{\DD}(p) p^{1/2-s} \nonumber \\
    &=& \frac{1-p^{1-2s}}{1- \chi_{\DD}(p) p^{1/2-s}} \quad \text{for odd $m$,} \label{eq:9}
\end{eqnarray}
using again that $\chi_{\DD}(p)^{2}=1$. Now we insert these formulas \eqref{eq:8} and \eqref{eq:9} into \eqref{eq:C-prod-local-L} and get the assertion with \eqref{eq:Lgamma-n-EPlocal}.
\end{proof}

Even though it is not important for our later application of the formula, we state, like our reference \cite{Bruinier2001Eisenstein}, the following
\begin{Cor}
  The Eisenstein coefficients $C(\gamma,n)$ are rational numbers.
\end{Cor}
This can be proven by using the functional equation for the Dirichlet $L$-series and the $\zeta$-function, followed by the evaluation at negative integers with Bernoulli polynomials. \cite{Zagier1981} is given as a reference.

\subsubsection[The special case $k=m/2$]{The special case \texorpdfstring{$k=\tfrac{m}{2}$}{k=m/2}}
\label{sec:C-gamma-n-k-mHALF}
As mentioned earlier, for later applications we only need the special case $k= \frac{m}{2}$.
We restate the last Theorem~\ref{thm:C-gamma-n-general-k} for this case, which will result in an even more explicit formula. The following Theorem~\ref{thm:C-gamma-n-k-mHALF} is used in the proof of Proposition~\ref{prop:C-lambda_1}. But the proposition's proof actually only needs formula~\eqref{eq:10} from the proof of Theorem~\ref{thm:C-gamma-n-k-mHALF}. Note that this doesn't use the Lemma from Siegel. So Theorem~\ref{thm:C-gamma-n-k-mHALF} is only given for completeness.
\begin{Thm}
  \label{thm:C-gamma-n-k-mHALF}
  Let $\gamma \in L'$ and $n \in \ZZ - q(\gamma), n>0$. Then the coefficient $C(\gamma,n)$ of the Eisenstein series $E(\tau)$ of weight $k= \frac{m}{2}$ is equal to
\begin{multline}
      C(\gamma,n) = \frac{2^{k+1} \pi^{k} n^{k-1} (-1)^{b^{+}/2}}{\sqrt{|L'/L|}\Gamma(k)}  \\
      \qquad
      \times \begin{dcases}
        \frac{\sigma_{1-k}(\tilde n,\chi_{4D})}{L(k,\chi_{4D})} \ \prod_{\mathclap{p| 2 \det S}} \ p^{w_{p}(1-2k)} N_{\gamma,n}(p^{w_{p}}), &
           \text{if $2\,|\,m$,} \\
        \frac{L(k- \tfrac{1}{2}, \chi_{\DD})}{\zeta(2k-1)} \sum_{d|f} \mu(d) \chi_{\DD}(d) d^{1/2-k} 
           \sigma_{2-2k} \big( \tfrac{f}{d} \big)
           \prod_{\mathclap{p | 2 \det S}} \frac{p^{w_{p}(1-2k)} N_{\gamma,n}(p^{w_{p}})}{1- p^{1-2k}}, & \text{if $2\notdivides m$}.
      \end{dcases}\label{eq:C-gamma-n-k-mHALF}
    \end{multline}
The quantities $D, \DD, f$ are defined as in Lemma~\ref{lemma:Siegel2} and $\sigma_{s}$ is the twisted divisor sum from Definition~\ref{def:twisted-div-sum}.
\end{Thm}
\begin{proof}
  Let $p$ be a prime. We evaluate the local Euler factors $L_{\gamma,n}(s=k,p)$ with $k= \frac{m}{2}$ and get
  \begin{equation*}
    L_{\gamma,n}(k,p) = N_{\gamma,n}(p^{w_{p}}) p^{w_{p}(1-2k)}
  \end{equation*}
We insert this into the formula \eqref{eq:C-gamma-n-general-k} for $C(\gamma,n)$ from the preceding theorem and get (here $\epsilon = 2 b^{+}$)
 \begin{multline}
      C(\gamma,n) = \frac{2^{k+1} \pi^{k} n^{k-1} (-1)^{b^{+}/2}}{\sqrt{|L'/L|}\Gamma(k)}  \\
      \qquad
      \times \begin{dcases}
        \frac{1}{L(k,\chi_{D})} \;\; \prod_{\mathclap{p| 2 \tilde n \det S}} \ \frac{p^{w_{p}(1-2k)} N_{\gamma,n}(p^{w_{p}})}{1-\chi_{D}(p)p^{-k}}, &
           \text{if $m$ is even,} \\
        \frac{L(k- \tfrac{1}{2}, \chi_{\DD})}{\zeta(2k-1)}\ \prod_{\mathclap{p|2 \tilde n \det S}}\
            \frac{1-\chi_{\DD}(p)p^{1/2-k}}{1-p^{1-2k}} p^{w_{p}(1-2k)} N_{\gamma,n}(p^{w_{p}}), & \text{if $m$ is odd}.
      \end{dcases}\label{eq:10}
    \end{multline}
\textbf{For even $m$} we evaluate the factors or the finite Euler product for primes $p$ dividing $\tilde n$ but not $2 \det S$, using \eqref{eq:Siegel-m-even} from Lemma~\ref{lemma:Siegel2} to
\[ \sigma_{1-k}(p^{v_{p}(n)}, \chi_{D}). \]
Note that the Dirichlet character $\chi_{4D}$ equals $\chi_{D}$ for all primes $p \geq 3$ because then 
\[ \left( \frac{4}{p} \right) = \left( \frac{2}{p} \right)^{2} =1.\]
For $p=2$ it vanishes because $(4/2)=0$.

We use this to write the finite Euler product for even $m$ as 
\begin{multline}
  \smashoperator{\prod_{\substack{p| \tilde n \\ p \notdivides 2 \det S }}} \sigma_{1-k}(p^{v_{p}(n)}, \chi_{4D}) \smashoperator{\prod_{p | 2 \det S}} \frac{p^{w_{p}(1-2k)} N_{\gamma,n}(p^{w_{p}})}{1-\chi_{D}(p)p^{-k}}  \\
  =  \sigma_{1-k}(\tilde n,\chi_{4D}) \frac{1}{1-\chi_{D}(2)2^{-k}} \smashoperator{\prod_{p|2 \det S}} p^{w_{p}(1-2k)} N_{\gamma,n}(p^{w_{p}}) \label{eq:11}
\end{multline}
where we used $\chi_{D}(p) = 0$ for $p | \det S | D$ and combined the left product to $\sigma_{1-k}(\tilde n,\chi_{4D})$.

\textbf{For odd $m$}, we use \eqref{eq:Siegel-m-odd} from Lemma~\ref{lemma:Siegel2} to write the finite Euler product over $p | 2 \tilde n \det S$ in \eqref{eq:10} as
\begin{equation}
   \smashoperator{\prod_{\substack{p| \tilde n \\ p \notdivides 2 \det S}}} \left( \sigma_{2-2k}(p^{v_{p}(f)}) - \chi_{\DD}(p) p^{(1/2-k)}\sigma_{2-2k}(p^{v_{p}(f)-1}) \right) \; 
     \smashoperator{\prod_{p| 2 \det S}} \frac{p^{w_{p}(1-2k)} N_{\gamma,n}(p^{w_{p}})}{1-p^{1-2k}}  \label{eq:12}
\end{equation}
where we used for $p | \DD$ that $\chi_{\DD}(p) = 0$.
Note that the factors in the left product equal to $1$ if $p \notdivides f$, furthermore that $\tilde n = \tilde n_{0} f^{2}$ and that $(f, 2 \det S)=1$.
So the product over $p \,|\, \tilde n, p \notdivides 2 \det S$ is the same as over $p|f$.
We expand the left product $\prod_{p|f}$ and see that it is the sum over all square-free integers $d | f$ with each summand being
\begin{multline}
  \prod_{p \notdivides d} \sigma_{2-2k} \big( p^{v_{p}(f)} \big) \mu(d)
    \prod_{p | d} \chi_{\DD}(p) p^{1/2-k} \sigma_{2-2k} \big( p^{v_{p}(f)-1} \big) \\
  = \mu(d) \chi_{\DD}(d) d^{1/2-k} \prod_{p \notdivides d} \sigma_{2-2k} \big( p^{v_{p}(f)} \big)
    \prod_{p | d} \sigma_{2-2k} \big( p^{v_{p}(f)-1} \big) \label{eq:13}
\end{multline}
where we directly applied the definition of the Moebius function $\mu$. Define
\[ d' := \prod_{p|d} p^{v_{p}(f)}. \]
Then the products become
\begin{equation*}
   \prod_{p \notdivides d} \sigma_{2-2k} \big( p^{v_{p}(f)} \big) = \sigma_{2-2k} \left( \frac{f}{d'} \right); \qquad
    \prod_{p | d} \sigma_{2-2k} \big( p^{v_{p}(f)-1} \big) = \sigma_{2-2k} \left( \frac{d'}{d} \right)
\end{equation*}
using that $\sigma_{s}(\cdot)$ is multiplicative (for coprime arguments).
Because $\frac{f}{d'}$ and $\frac{d'}{d}$ are also coprime,
the product of the both equals $\sigma_{2-2k} \big( \frac{f}{d} \big)$.
Thus \eqref{eq:13} equals
\begin{equation*}
  \mu(d) \chi_{\DD}(d) d^{1/2-k} \sigma_{2-2k} \left( \frac{f}{d} \right)
\end{equation*}
and hence the finite Euler product, which we earlier evaluated as \eqref{eq:12}, becomes
\begin{equation*}
  \sum_{d | f} \mu(d) \chi_{\DD} (d) d^{1/2-k} \sigma_{2-2k} \left( \frac{f}{d} \right)
     \prod_{p| 2 \det S} \frac{p^{w_{p}(1-2k)} N_{\gamma,n}(p^{w_{p}})}{1-p^{1-2k}}.
\end{equation*}
Inserting this and resp.\ \eqref{eq:11} into \eqref{eq:10} now yields the assertion.
\end{proof}