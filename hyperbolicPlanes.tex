
\section{Hyperbolic planes}
\label{sec:hyperbolic-planes}

In the last chapter~\ref{cha:upper-bounds-eisenst} we want to define a good test vector to minimize certain Eisenstein coefficients for this vector.
It will lie inside a hyperbolic plane which is part of the dual lattice.
This short section deals with the question in how far we find hyperbolic planes inside a given lattice.

\begin{Def}\label{def:hyperbolicPlane}
  A \emph{(rescaled) hyperbolic plane} $H(N)$ denotes a two-dimensional lattice inside the quadratic space $(V,q)$, generated over $\ZZ$ by the two vectors $f_{1}$ and $f_{2}$, which satisfy $(f_{1},f_{2}) = N \in \NN$ and $q(f_{1}) = q(f_{2}) = 0$.
Such a basis will be called \emph{hyperbolic basis}.
The Gram matrix of $H(N)$ is 
\[ \left(
  \begin{array}{cc}
    0 & N \\ N & 0
  \end{array}
\right) \]
Hyperbolic planes have signature $(1,1)$ and are isotropic since the two base vectors $f_{1}$ and $f_{2}$ are.
We simply write $H$ for $H(1)$.
\end{Def}
\begin{Def}
  Let $L_{1} \subset (V_{1},q_{1})$ and $L_{2} \subset (V_{2},q_{2})$ be two lattices.
  Its \emph{orthogonal sum} $L_{1} \perp L_{2}$ is the lattice $L_{1} + L_{2}$ in the quadratic space
  $(V_{1} \oplus V_{2}, q)$ with the quadratic form $q(v_{1},v_{2}) := q_{1}(v_{1}) + q_{2}(v_{2})$.
\end{Def}

Assume $V$ is an indefinite quadratic space which is isotropic.
Then a lattice $L \subset (V,q)$ is not necessarily isotropic.
For example, take $L = \ZZ^{2} \subset (\RR^{2},q)$ with
\[ q(x_{1},x_{2}) = {x_{1}}^{2} - d {x_{2}}^{2}, \quad d \textnormal{ a non-square natural number.} \]
Then over $\ZZ^{2}$, $q(x_{1},x_{2}) =0$ only has the trivial solution, so $L$ is anisotropic.

However, this phenomenon only holds for small dimensions:
\begin{Thm}\label{thm:hasse-minkowski}
\textbf{(Hasse-Minkowski)}
Let $L \subset (V,q)$ be a lattice in an indefinite quadratic space of dimension $m \geq 5$. Then L is isotropic.
\end{Thm}
\begin{Def}
  For a lattice $L \subset (V,q)$ let the \emph{$\QQ$-space of $L$} be the space
  \[ V_{\QQ} = L \otimes_{\ZZ} \QQ \]
\end{Def}
The theorem yields the following
\begin{Lem}\label{lem:split-hypPl}
  If $L \subset (V,q)$ is an isotropic lattice, then it \emph{splits a hyperbolic plane} over $\QQ$.
  That is, the $\QQ$-space of L is isomorphic to the $\QQ$-space of $H \perp L_{0}$ for a lattice $L_{0}$.
\end{Lem}
Now applying the Hasse-Minkowski Theorem~\ref{thm:hasse-minkowski} twice gives the important
\begin{Cor}\label{cor:split2hypPlanes}
   Let $L$ be a lattice of signature $(2,l)$ and $l \geq 5$. Then it splits two hyperbolic planes over $\QQ$.
\end{Cor}