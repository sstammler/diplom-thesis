\section{The Picard group}
\label{sec:picard-group}

We will formulate the main result in terms of the Picard group of $Y(\Gamma)$.

If $\Gamma$ acts freely on $\HH_{l}$, $Y(\Gamma)$ is non-singular and we let $\Pic (Y(\Gamma))$ denote the algebraic \emph{Picard group}, i.e.,\ the group of line bundle equivalence classes. In the case $l \geq 3$ it is finitely generated.

If the action of $\Gamma$ is not free on $\HH_{l}$, choose a normal subgroup $\Gamma' \subset \Gamma$ of finite index that acts freely on $\HH_{l}$ and define
\[ \Pic (Y(\Gamma)) := \Pic (Y(\Gamma'))^{\Gamma/ \Gamma'}, \]
i.e.,\ as the group of $\Gamma/ \Gamma'$-invariant elements of $\Pic(Y(\Gamma'))$. This definition is independent of the choice of $\Gamma'$. Fortunately, $\Gamma_{L}(N)$ is guaranteed to act freely for $N \geq 3$, see \cite[Hilfssatz~6.5,~Ch.~2.6]{Freitag1983}.

\subsection{Canonical bundle}
\label{sec:canonical-bundle}

Let $\calC$ be the determinant line bundle of holomorphic $l$-forms, also called the \emph{canonical bundle}. It spans a one dimensional subspace in
\[ \PicQ(Y(\Gamma)) := \Pic(Y(\Gamma)) \otimes_{\ZZ} \QQ. \]
An element of $\PicQ(Y(\Gamma))$ is called \emph{non-trivial} if it is not contained in the span of the canonical bundle and the group $\PicQ(Y(\Gamma))$ is called \emph{non-trivial} if it contains such an element. It can be shown that 
\begin{Prop}\label{prop:C-EQ-cuspForms}
  The canonical bundle $\calC$ is equal to $\calS_{l}$ in $\PicQ(Y_{L})$.
\end{Prop}
See \cite[Lem.~5.10]{Bruinier2002} for details.

\subsection{Degrees of divisors}
\label{sec:degrees-divisors}

Let $\Omega$ be the first chern form on $M_{1}$. Up to a positive multiple this is the $(1,1)$-form on $\HH_{l}$ corresponding to the unique $O'(V)$-invariant Kähler metric, cf.\ \cite{Bruinier2002a,Bruinier2007}. Now let
\[ \vol (Y(\Gamma)) := \int_{\mathrlap{Y(\Gamma)}} \;\; \Omega^{l} \]
be the volume of $Y(\Gamma)$ and for a divisor $D \in \Div (Y(\Gamma))$ we define its \emph{analytic degree} by
\[ \deg (D) := \deg_{Y(\Gamma)} (D) := \int_{D} \Omega^{l-1}. \]
In \cite[Th.~1~\&~\ppno~10]{Bruinier2007} Bruinier shows the following proposition. which can be seen as a generalization of the classic $k/12$-formula to orthogonal modular forms.
\begin{Prop}\label{prop:DegDivEQkVol}
  If $F \in \calM_{k}(\Gamma,\chi)$ for some multiplier system $\chi$ and $\ddiv (F)$ is the divisor of $F$, then
  \[ \deg_{Y(\Gamma)} (\ddiv (F)) = k \vol (Y(\Gamma)) \]
\end{Prop}
He remarks that this immediately follows from the Poincaré-Lelong formula if $Y(\Gamma)$ is compact, which is, in general, not the case. He proves the general case.

Note that the right hand side is constant for the same linear equivalence class of $\ddiv (F)$ and hence does the analytic degree of a divisor only depend on its linear equivalence class.

Combining this proposition and \ref{prop:singular-weights} we get
\begin{Prop}\label{prop:ineqDegDiv}
  If $F \in \calM_{k}(\Gamma,\chi)$ is non-constant, then
  \[ \deg (\ddiv (F)) \geq (l/2 - 1) \vol(Y(\Gamma)) \]
\end{Prop}
Now Proposition~\ref{prop:C-EQ-cuspForms} comes into play.
\begin{Cor}\label{cor:divisorIneq}
  If $D$ is a non-zero effective divisor on $Y(\Gamma)$ whose corresponding line bundle $\calL(D)$ is contained in the span of the canonical bundle $\calC$ in $\PicQ(Y(\Gamma))$, then
\[ \deg (D) \geq (l/2 -1) \vol (Y(\Gamma)) \]
\end{Cor}
\begin{proof}
  We may assume that $\Gamma$ acts freely. Since $\calL(D)$ lies in the span of $\calC$ in $\PicQ(Y(\Gamma))$, there are $a$, $b \in \NN$, s.t.
\[ \calL(D)^{\otimes a} = \calC^{\otimes b} = \calS_{lb} \]
in $\Pic(Y(\Gamma))$. This means that there is a cusp form $F$ of rational weight $bl/a$ with some multiplier such that $\ddiv (F) = D$. The preceding Proposition~\ref{prop:ineqDegDiv} concludes the proof.
\end{proof}