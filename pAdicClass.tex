\section[On the classification of quadratic forms]{On the classification of quadratic forms over the \texorpdfstring{$p$}{p}-adic integers}
\label{sec:class-quadr-forms}
I will state a few results about the classification of quadratic forms over the $p$-adic integers $\ZZ_{p}$. Because $\ZZ_{p} / p^{k} \ZZ_{p}$ is isomorphic to $\ZZ/ p^{k} \ZZ$, this enables us to analyze the number of solutions of certain congruences of quadratic forms over the sets $\ZZ/p^{k} \ZZ$ and in particular over $\ZZ / 8 \ZZ$.

\begin{Def}
  Let $p$ be a prime or $\infty$ (some authors use $-1$ instead). Two lattices $L$ and $M$ with their corresponding quadratic forms are called \emph{$p$-adically equivalent}, in symbols $L \sim_{p} M$, or $L \cong M$ if $p$ is clear from the context, if the extended lattices $L \otimes_{\ZZ} \ZZ_{p}$ and $M \otimes_{\ZZ} \ZZ_{p}$ are isomorphic over $\ZZ_{p}$.
\end{Def}
There is the fundamental
\begin{Thm}\label{thm:p-adic-equivalence}
  Every integral lattice is $p$-adically equivalent to a lattice with diagonal Gram matrix, if $p \neq 2$. For $p=2$ there is a $2$-adic equivalence to a lattice whose Gram matrix has either $1 \times 1$-blocks of the form $(p^{l}\varepsilon)$, $l \geq 0$, $\varepsilon$ a unit in $\ZZ/ 8\ZZ$, or $2 \times 2$-blocks of the form
  \begin{equation}\label{eqn:2-adicMatrices}
    p^{l} \begin{pmatrix}
      0 & 1 \\ 1 & 0
    \end{pmatrix} 
    \quad\text{or}\quad
    p^{l} \begin{pmatrix}
      2 & 1 \\ 1 & 2
    \end{pmatrix},\, l \geq 0
  \end{equation}
on its diagonal. We get the (finite) $p$-adic orthogonal decomposition
\begin{equation}
  \label{eqn:p-adicDecomp}
  L \sim_{p} p^{0} L_{0} \perp p^{1} L_{1} \perp \ldots
\end{equation}
into lattices $L_{i}$ with quadratic forms whose Gram determinants are not divisible by $p$.
\end{Thm}
I refer to \cite[Lem~4.1, \pno~117]{Cassels2008} and \cite[\ppno~76]{Kitaoka1993}. An easy consequence is
\begin{Lem}\label{lem:evenDet-oddDim}
  If the dimension of an even lattice is odd, its Gram determinant must be divisible by $2$.
\end{Lem}
\begin{proof}
  Suppose $L$ is even and its determinant is odd. Consider the $2$-adic decomposition~\eqref{eqn:p-adicDecomp}. Because the determinant is odd, it is $L_{i}=0$ for $i \geq 1$. The Gram matrix of the component $L_0$ can only contain the $2 \times 2$ matrices~\eqref{eqn:2-adicMatrices}, because the lattice is even. But then the dimension must be even.
\end{proof}
I also refer to \cite[(2.9),~\pno~9]{Kneser2002} for another more fundamental explanation that doesn't involve $p$-adic ideas and just considers the polynomial of the determinant.