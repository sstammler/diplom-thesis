## SETUP #############
# Comment or uncomment the respective sections for setting $k_L$ and $\delta_2$=d_2.
# Options for calculating the $N_1$, i.e. we don't if $L_1$ rescales:
#k_L(k) = k  # $k_L=k$ in this case and
#d_2(N) = 1  # the exponent $\delta_2$ is always $1$.
# Options for calculating the $N_0$, i.e. we know that $L_1$ is also rescaled by $N$:
k_L(k) = 2*k - 1                     # $k_L=2k-1$ in this case and
d_2 = lambda N: 0 if (N == 2) else 1 # the exponent $\delta_2$ is $1$ except for $N=2$.

## HELPER FUNCTIONS ##
# The remaining constants as functions in $N$
s         = lambda N: 2 if N<3 else 1 # $s_{\lambda}$ is $1$ for $N=1,2$, and $2$ otherwise.
K_N       = lambda N: 1 if (N % 2 == 0) else 2 # The constant $K_N$.
# The upper bound for even $l$ as a function in $k,N$.
EC_l_even = lambda k,N: 2^(k+1) * pi^k * zeta(k) * K_N(N) / \
                        ( N^(k_L(k)) * gamma(k) * zeta(2*k) )
# The upper bound for odd $l$ as a function in $k,N$.
EC_l_odd  = lambda k,N: 2^(k+1) * pi^k * zeta(k-1/2) * zeta(2*k-1) * K_N(N) / \
                        ( N^(k_L(k)) * gamma(k) * zeta(4*k-2) * sqrt(2)^(d_2(N)) )
# The upper bound for $-C(\lambda,-q(\lambda))$ as a function in $l(=2k-2)$ and $N$:
EC = lambda l,N: EC_l_even(l/2+1, N) if (l % 2 == 0) else EC_l_odd(l/2+1, N)

## ITERATIONS ########
def find_smallest_N(l): # For a given $l$, this function iterates over $N$ and
            # calculates the values of the upper bound for $-C(\lambda,-q(\lambda))$.
    N = 1   # The iteration starts at $N=1$.
    while EC(l,N) >= s(N)*(l-2): # As long as the bounds are $\geq s_{\lambda}(l-2)$
        N += 1 # we iterate over $N$ and increase it by $1$ at each step.
    else:   # When we found a value $N$ for which the bound fell below $s_{\lambda}(l-2)$
        print l, N, bool(EC(l,N+1) < s(N+1)*(l-2)) ,n(EC(l,N)) # we print it
            # together with $l$, the value of the bound for $l,N$ and check
            # whether the bound would also fell below $s_{\lambda}(l-2)$ for the next $N$.
            # This is necessary as the bounds are only decreasing in $N$
            # for a given parity (mind $K_N$).

for l in range(3,35): # Finally, we run this process for every $l\geq3$ up to $34$.
    find_smallest_N(l)
