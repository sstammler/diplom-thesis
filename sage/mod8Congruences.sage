#import sys
import random

#totalcount = 8^7
#progress=0 # we divide in 64 steps
#count=0
#P=0

l=3
det_mod=4

#R = mrange([8,8,1,1,8,8,1,8,8,8]) # l=4
R= mrange([8,8,1,8,8,8]) #l=3
random.shuffle(R) # shuffling the set of upper triangular matrices should increase time to find a bad quadratic form
print 'shuffled set of quadratic forms generated'

#for D in mrange([8,8,1,1,8,8,1,8,8,8]):
@parallel(ncpus=3)
def checkSolutions(D):
#    count += 1 # counter for progress
    q = QuadraticForm(ZZ,l,D)
    if q.det() % det_mod != 0:
### monitoring progress ###
#        if count/totalcount > progress:
#            progress += 1/64
#            if progress*64 % 8 == 0:
#                P += 1
#                print P
#            else: 
#                sys.stdout.write('.')
#                sys.stdout.flush()
###########################
        soln = q.count_congruence_solutions(2, 3, 1, None, None)
        if soln > (8^l)/2:
            print 'Found %s solutions,i.e., more than half' % soln
            print 2*soln/(8^l), q.det()
            print q.Hessian_matrix()
            print '--------------------------------'
            return q.Hessian_matrix()

#v=list(checkSolutions(R))

