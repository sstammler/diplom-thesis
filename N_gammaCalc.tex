\subsection{Calculations for \texorpdfstring{$N_{\gamma,n}$}{Nɣ,n(a)}}
\label{sec:calcs-N_gamma_n}
In this section we will deduce more explicit formulas for calculating $N_{\gamma,n}$ and in particular find a simple relation of $N_{\gamma,n}(p^{\alpha})$ for prime powers, given that $\alpha$ is sufficiently large, see Lemma~\ref{lem:Ngamma_alphaLarge}.

Fix a basis $\BB$ of $L$ and write $S=S(\BB,L)$ for its Gram matrix, cf.\ \ref{sec:gram-matr-sign}. Using \eqref{eq:GramQuadrForm} we can reformulate the definition of $N_{\gamma,n}$ as
\begin{equation}
  \label{eq:Ngamma-nGram1}
  N_{\gamma.n}(a) = \# \setb{r \in (\ZZ / a \ZZ)^{m}}{\tfrac{1}{2} S [r - \gamma] + n \equiv 0 \pmod{a}}
\end{equation}
\begin{Def}
  The \emph{level} of $\gamma \in L'$ is defined as
\[ d_{\gamma} := \min \setb{b \in \NN}{ b \gamma \in L} \]
\end{Def}
and we also set $\tilde \gamma := d_{\gamma} \gamma$ and $\tilde n := d_{\gamma}^{2}n$.
Obviously $\tilde \gamma \in L$ and because of
\[ \ZZ \ni q(\tilde \gamma) = d_{\gamma}^{2} q(\gamma) \equiv \tilde n \pmod{1}\]
$\tilde n$ is integral. $2 n d_{\gamma}$ is integral as well because $2 q(\gamma) d_{\gamma} = (d_{\gamma}\gamma,\gamma) \in \ZZ$. Also $d_{\gamma}$ divides $\det(S)$.
With these definitions we may reformulate $N_{\gamma,n}$ as following. If $a$ and $\det(S)$ are coprime, then $d_{\gamma}$ is invertible modulo $a$ and we can multiply the congruence in \eqref{eq:Ngamma-nGram1} by $d_{\gamma}^{2}$ to obtain
\begin{IEEEeqnarray}{rCllCl}
  N_{\gamma.n}(a) & = &
     \# \setb{r \in (\ZZ / a \ZZ)^{m} }{&\tfrac{1}{2} S [d_{\gamma} r - \tilde \gamma] & \equiv & - \tilde n \pmod{a}} \nonumber \\
     & = & \# \setb{r \in (\ZZ /  a \ZZ)^{m}}{&\tfrac{1}{2} S [r] & \equiv & - \tilde n \pmod{a}} \label{eq:NgammaCoprime}
\end{IEEEeqnarray}
where the second equality follows from the fact that $r \mapsto d_{\gamma} r - \tilde \gamma$ is a bijection on $(\ZZ /  a \ZZ)^{m}$.

For general $a$ we multiply the congruence \eqref{eq:Ngamma-nGram1} by $d_{\gamma}^{2}$ and get
\[  N_{\gamma.n}(a) =
     \# \setb{r \in (\ZZ / a \ZZ)^{m} }{\tfrac{1}{2} S [d_{\gamma} r - \tilde \gamma] \equiv - \tilde n \pmod{d_{\gamma}^{2}a}}. \]
Observe that the set  $d_{\gamma} (\ZZ / a \ZZ)^{m} - \tilde \gamma$ equals the set
$\setb{r \in (\ZZ / d_{\gamma}a \ZZ)^{m}}{r \equiv \tilde \gamma \pmod{d_{\gamma}} }$ and hence
\begin{equation}
  \label{eq:NgammaGeneral}
  N_{\gamma.n}(a) =
     \# \setb{r \in (\ZZ / d_{\gamma} a \ZZ)^{m} }{\tfrac{1}{2} S [r] \equiv - \tilde n \pmod{d_{\gamma}^{2}a},\quad 
       r \equiv \tilde \gamma \pmod{d_{\gamma}}}.
\end{equation}

Furthermore it is clear from the definition that $N_{\gamma,n}$ is multiplicative, that is, for coprime $a_{1},a_{2}$ it is
\[ N_{\gamma,n}(a_{1}a_{2}) = N_{\gamma,n}(a_{1}) N_{\gamma,n}(a_{2}) \]
which yields an Euler Product expansion for $L_{\gamma,n}$,
\begin{equation}
  \label{eq:Lgamma-n-EulerProd}
  L_{\gamma,n} (s) = \prod_{p \text{ prime}} \left( \sum_{\nu \geq 0} N_{\gamma,n}(p^{\nu}) p^{\nu (1- m/2 -s)} \right).
\end{equation}

For a prime $p$ let $v_{p}: \QQ \rightarrow \ZZ$ be the additive $p$-adic valuation and define
\[ w_{p} := 1+2v_{p}(2nd_{\gamma}) \]
which is always $\geq 1$ as $2nd_{\gamma}\in \ZZ$.

The following lemma is a reformulation of \cite[Hilfssatz~13, \pno~542]{Siegel1935}.
\begin{Lem}\label{lem:Ngamma_alphaLarge}
  Let $\alpha \geq w_{p}$, then
  \begin{equation}
    \label{eq:NgammaPrime-powers}
    N_{\gamma,n}(p^{\alpha+1}) = p^{m-1}N_{\gamma,n}(p^{\alpha})
  \end{equation}
\end{Lem}
With this lemma we can rewrite the Euler product for $L_{\gamma,n}$ in a more computable way as
\begin{IEEEeqnarray}{rCl}
  L_{\gamma,n}(s) &=& \prod_{p}\left( \sum_{\nu=0}^{w_{p}-1} N_{\gamma,n}(p^{\nu}) p^{\nu(1-m/2-s)} +
     N_{\gamma,n}(p^{w_{p}})p^{w_{p}(1- m/2 -s)} \sum_{\nu \geq 0} p^{\nu (m/2 -s)} \right) \nonumber \\
 &=& \zeta(s-m/2) \prod_{p} L_{\gamma,n}(s,p) \label{eq:Lgamma-n-EPlocal}
\end{IEEEeqnarray}
with the local Euler factors
\begin{equation}
  \label{eq:Lgamma-n-p}
  L_{\gamma,n}(s,p) = \big( 1- p^{m/2-s} \big)
    \sum_{\nu=0}^{w_{p}-1} N_{\gamma,n}(p^{\nu})  p^{\nu(1-m/2-s)} + N_{\gamma,n}(p^{w_{p}}) p^{w_{p}(1-m/2-s)}.
\end{equation}
For the next lemma, we need the following
\begin{Def} \label{def:twisted-div-sum}
Let $D$ be a discriminant, i.e., $D$ is a non-zero integer with $D \equiv 0$ or $1 \pmod{4}$.
Then we define the \emph{Dirichlet character modulo} $|D|$ via the Kronecker symbol, $\chi_{D}(a) = \big( \frac{D}{a} \big)$ and denote the corresponding $L$-series by $L(s,\chi_{D})$.
For $n\in \NN$ and $\chi$ a Dirichlet character we then define the \emph{twisted divisor sum} by
\[ \sigma_{s}(n,\chi) = \sum_{d|n}\chi(d) d^{s}, \]
where we sum over all positive divisors of $n$. For the trivial character $\chi_{1}$ modulo $1$, we just write
$\sigma_{s}(n) := \sigma_{s}(n,\chi_{1})$ and for $x \in \RR \setminus \NN$ we define $\sigma_{s}(x,\chi)=0$.
\end{Def}

\begin{Lem}\textbf{(Siegel)}\label{lemma:Siegel2}
Suppose $p$ is a prime not dividing $2 \det S$ and $\alpha \in \ZZ, \alpha > v_{p}(n)$.
\begin{enumerate}[(i)]
  \item If $m$ is even, put $D := (-1)^{m/2} \det S$ and it is
    \begin{IEEEeqnarray}{rCl}
      p^{\alpha(1-m)} N_{\gamma,n}(p^{\alpha}) & = & \big( 1 - \chi_{D}(p) p^{-m/2} \big)
         \left( \sum_{j=0}^{v_{p}(n)} \chi_{D}(p^{j}) p^{j(1-m/2)} \right) \nonumber \\
         &=& \big( 1 - \chi_{D}(p) p^{-m/2} \big) \sigma_{1-m/2}\big( p^{v_p (n)}, \chi_D \big) \label{eq:Siegel-m-even}
    \end{IEEEeqnarray}
  \item If $m$ is odd, write $n=n_{0}f^{2}$ with $n_{0} \in \QQ, f \in \NN$ such that
    \begin{itemize}
    \item $f$ is coprime to $2 \det S$ and
    \item if $\ell$ is a prime coprime to $2 \det S$, then $v_{\ell}(n_{0}) \in \{0,1\}$.
    \end{itemize}
    Define $\tilde n_{0} := n_{0} d_{\gamma}^{2}$ and $\DD := 2 (-1)^{(m+1)/2} \tilde n_{0} \det S$.

    If $m \geq 3$
    \begin{multline}
      p^{\alpha(1-m)} N_{\gamma,n}(p^{\alpha}) \\
      = \frac{1- p^{1-m}}{1 - \chi_{\DD}(p) p^{(1-m)/2}} \left( \sigma_{2-m}(p^{v_{p}(f)}) 
          - \chi_{\DD}(p) p^{(1-m)/2}\sigma_{2-m}(p^{v_{p}(f)-1}) \right).\label{eq:Siegel-m-odd}
    \end{multline}
    If $m=1$, it is just
    \begin{equation*}
      N_{\gamma,n}(p^{\alpha}) = \big( \chi_{\DD} (p) + \chi_{\DD}(p)^{2} \big) p^{v_{p}(f)}.
    \end{equation*}
    Furthermore $D$ and $\DD$ are discriminants because $(-1)^{m/2}\det S \equiv 0, 1 \pmod{4}$, if $m$ is even
    and $\det S \equiv 0 \pmod{2}$, if $m$ is odd.
\end{enumerate}
\end{Lem}
Because here $p$ is coprime to $\det S$, $N_{\gamma,n}$ is given by \eqref{eq:NgammaCoprime}. Then the theorem is a reformulation of \cite[Hilfssatz~16, \pno~544]{Siegel1935}.
