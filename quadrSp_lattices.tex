\chapter{Lattices and quadratic Forms}
\label{chap:quadr-spac-latt}

I start with a short section about lattices and quadratic forms, as these are part of the fundamental language used in this thesis. I loosely follow \cite{Bundschuh2001}.

\section{Quadratic modules and spaces}
\label{sec:quadratic-modules-spaces}
\begin{Def}
  Let $M$ be a finitely generated $R$-module, where $R$ is a unitary commutative ring. A \emph{quadratic form} on $M$ is a mapping
  \[ q: \quad M \longrightarrow R \]
  such that
\begin{enumerate}[(i)]
\item $q(\alpha x) = \alpha^{2} q(x) \qquad$ for all $\alpha \in R, \, x \in M,$
\item the mapping $(\cdot,\cdot): M \times M \longrightarrow R$ defined by
  \[ (x,y) := q(x+y) - q(x) - q(y) \]
  is $R$-bilinear. It is called the \emph{bilinear form} associated to $q$.
\end{enumerate}
A module $M$ together with a quadratic form $q$ is then called a \emph{quadratic module over $R$}, denoted by $(M,q)$.
If $M=V$ is a vector space, i.~e. $R$ is a field, we call $(V,q)$ a \emph{quadratic space}.
further we use the following definitions
\begin{enumerate}[(i)]
\item Two elements $x,y \in M$ are called \emph{orthogonal} if $(x,y) = 0$.
\item The \emph{orthogonal complement} of a subset $A \subseteq M$ is
  \[ A^{\perp} := \{ x \in M \, | \, \forall y \in M : \, (x,y) = 0 \} \]
  and we abbreviate $\{x\}^{\perp} =: x ^{\perp}$. 
  Note that $A^{\perp} = \bigcup_{x \in A} x^{\perp}$.
  Every orthogonal complement is a submodule, resp.\ a subspace if $M$ is a vector space.
\item The quadratic module or space and the corresponding quadratic and bilinear form are called \emph{non-degenerate} if $M^{\perp}=\{0\}$.
\item An element $x \in M \setminus \{0\}$ is called \emph{isotropic} if $q(x)=0$ and \emph{anisotropic} if $q(x) \neq 0$. The module is called isotropic if it contains an isotropic element, anisotropic otherwise.
\item The quadratic space $(V,q)$ is called \emph{positive definite}, resp.\ \emph{negative definite} if $q(x)>0$, resp.\ $q(x)<0$ for all non-zero $x$. Otherwise it is called \emph{indefinite}.
\end{enumerate}
\end{Def}
\begin{Rmk}
  Given a (non-degenerate) bilinear form $(\cdot,\cdot)$, we get the corresponding (non-\-de\-ge\-ne\-rate) quadratic form with $q(x):= \frac{1}{2}(x,x)$, given that $2$ is invertible in $R$.
\end{Rmk}

\section{Lattices}
\label{sec:lattices}

Now we will define lattices and some related vocabulary.
\begin{Def}
  A \emph{lattice} is a quadratic module over $\ZZ$ whose quadratic form takes values in $\QQ$.
  It is called \emph{integral} if the bilinear form associated to $q$ takes values in $\ZZ$ and \emph{even} if $q$ itself takes only values in $\ZZ$.
\end{Def}
\begin{Def}
  If $L$ lies in a quadratic space $(V,q)$, there is a $\ZZ$-basis $\BB = \{ e_{1}, \ldots, e_{m} \}$ for $L$ such that
\[ L = \sum_{i=1}^{m} \ZZ e_{i} . \]
We call $\BB$ a \emph{lattice basis} and $m$ the \emph{dimension} or \emph{rank} of the lattice.
\end{Def}

\subsection{Gram matrix and signature}
\label{sec:gram-matr-sign}

\begin{Def}
  The \emph{Gram matrix} of a lattice $L$ is the matrix
  \[ S = S(L) = S(L,\BB) = (\, (e_{i},e_{j}) \,)_{1\leq i,j \leq m} \]
  for a lattice basis $\BB$.
\end{Def}
We see that $S$ is an integral matrix for integral lattices an the diagonal only contains even numbers for even lattices.

\begin{Lem}
  Let $x,y \in L$ and use the same variables for their coordinate vectors in a given basis $\BB$. Then we may calculate
  \begin{IEEEeqnarray}{rCl}
    (x,y) & = & x^{t} S y \qquad \text{and} \label{eq:GramBilForm} \\
    q(x)  & = & \tfrac{1}{2} S[x] := \tfrac{1}{2} x^t S x \label{eq:GramQuadrForm}
  \end{IEEEeqnarray}
\end{Lem}

Even though the Gram matrix depends on the choice of the lattice basis $\BB$, this choice does not matter for many applications so we will omit the $\BB$ in most situations.
\begin{Def}
  The determinant of the lattice $\det (L)$ is defined as the determinant of the Gram matrix,
\[ \det (L) := \det (S) = \det (S(L,\BB)) \]
for any basis $\BB$. It is independent of the choice of $\BB$.
\end{Def}

Every real symmetric matrix can be diagonalized by an orthogonal matrix, so the following definition makes sense:
\begin{Def}
  For a non-degenerate lattice the signature $(b^{+}, b^{-})$ is the number $b^{+}$ of positive and $b^{-}$ of negative entries on the diagonal of any diagonalized Gram matrix. We have $m = b^{+}+ b^{-}$. This definition is independent of the choice of the Gram matrix.

For a real or rational quadratic space we can choose any basis and then define Gram-matrices and the signature in the same way. Clearly the signature of a non-degenerate lattice $L \subset (V,q)$ is the same as the signature of $V$.
\end{Def}

\subsection{Dual lattice}
\label{sec:dual-lattice}

Let $L \subset (V,q)$ be an integral lattice.
\begin{Def}
  The dual lattice $L'$ of $L$ is
  \[ L' = \{ \gamma \in V \,|\, \forall \lambda \in L : \, (\gamma, \lambda) \in \ZZ \}. \]
\end{Def}
We sum up some properties of the dual lattice.
\begin{Lem}
  \begin{enumerate}[(i)]
  \item $(L',q)$ is a lattice itself and has the same dimension as $L$.
  \item $L$ is contained in $L'$.
  \item For any lattice basis $\BB$ of $L$, which is a basis of $V$ as well, the Gram matrix $S=S(\BB,L)$ naturally induces a linear mapping on $V$ in $\BB$-coordinates. We have
    \begin{equation}
      \label{eq:1}
      L' = S^{-1}(L)
    \end{equation}
    and $S^{-1}$ is a Gram matrix for $L'$.
  \end{enumerate}
\end{Lem}
\begin{Def}
  For an even lattice $L$ we define the \emph{level} to be 
\[ N = \min \setb{b \in \NN}{\forall \gamma \in L' :\, b q(\gamma) \in \ZZ}  \]
\end{Def}

\subsection{The discriminant group}
\label{sec:discriminant-group}

To use the structure of a lattice, its discriminant group is key.
\begin{Def}
  Let $L$ be an integral lattice. Its \emph{discriminant group} is the quotient group $L'/L$.
\end{Def}
And again, some properties we are going to use.
\begin{Lem}
  \begin{enumerate}[(i)]
  \item Because of the relation $L'= S^{-1} L$ from Lemma \eqref{eq:1}, the discriminant group has order
    \[ \left| L'/L \right| = | \det L | \]
  \item If $L$ is integral, the mapping 
    $L'/L \, \times \, L'/L \rightarrow \QQ / \ZZ$
induced by the bilinear form of $L$ (via modulo $1$ reduction) is well defined.

If $L$ is additionally even, then the mapping $L'/L \rightarrow \QQ / \ZZ$ induced by the quadratic form $q$ of $L$ is well defined as well and is called \emph{discriminant form}.
  \end{enumerate}
\end{Lem}
