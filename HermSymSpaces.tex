
\section{Hermitian symmetric spaces for the orthogonal group}
\label{sec:herm-symm-spac}
In this section we will define different realizations of the Hermitian symmetric space for the orthogonal group $O(2,l)$. These can be seen as generalizations of the usual upper half plane $\HH$.

As introduced in chapter~\ref{chap:quadr-spac-latt}, let $(L,q)$ be an even lattice of signature $(2,l)$ for the quadratic form $q$ that lies in the quadratic space $(V,q)$, i.e., $V=L \otimes_{\ZZ} \RR$. If $O(V) \cong O(2,l)$ denotes the orthogonal group of $V$,
\[ O(V) = \setb{\varphi \in \operatorname{Aut} (V)}{q(\varphi(x)) = 
\varphi(x) \text{ for all } x \in V} \]
then the connected component of the identity in $O(V)$, denoted by $O'(V)$, is an index $2$ subgroup of $O(V)$. One can also define $O'(V)$ to be the subgroup of $O(V)$ whose spinor norm equals the determinant, see \cite{Bruinier2008}.
If $K \subset O(V)$ is a maximal compact subgroup, then $O(V)/K$ is a Hermitian symmetric space. It has a complex structure because we are in the special case of signature $(b^{+},b^{-})$ with $b^{+} = 2$. The other case where this would be Hermitian is $b^{-} = 2$. We briefly introduce the Grassmannian Model.

\subsection{The Grassmannian model}
\label{sec:grassmannian-model}

Consider the Grassmannian
\[ \Gr(V) = \setb{v \subset V}{ \dim v = 2 \text{ and } q|_{v} > 0}. \]
The stabilizer $K$ of a fixed element $v_{o} \in \Gr(V)$ is a maximal compact subgroup, having the form $K \cong O(2) \times O(n)$. So $Gr(V) \cong O(V)/K$ is a realization of the Hermitian symmetric space. This description may be easy, but it doesn't reveal the complex structure.

\subsection{The projective model}
\label{sec:projective-model}

This will be the model we work with most of the time and it is naturally endowed with a complex structure. Let $V(\CC) := V \otimes_{\RR} \CC$ be the complexification of $V$ and
\[ P(V(\CC)) = (V(\CC) \setminus \{0\}) / \CC^{*} \]
the corresponding projective space. We extend the bilinear form $(\cdot,\cdot)$ on $V$ $\CC$-bilineary to $V(\CC)$ and again write $(\cdot,\cdot)$. Furthermore write $[Z] \in P(V(\CC))$ for the equivalence class of $Z \in V(\CC)$. Then the space
\[ \calN = \setb{[Z] \in P(V(\CC))}{ (Z,Z) = 0}, \]
called the zero quadric, is a closed algebraic subvariety. Now consider the subset
\[ \calK := \setb{[Z] \in \calN}{ (Z,\bar Z) > 0} \]
which is a complex $l$-dimensional manifold. It consists of two connected components. the orthogonal group $O(V)$ acts transitively on $\calK$. The subgroup $O'(V)$ preserves the components of $\calK$ and $O(V) \setminus O'(V)$ interchanges them. Fix one component of $K$ and call it $\HH_{l}$. Now $O'(V)$ act transitively on $\HH_{l}$. The set $\HH_{l}$ is indeed a realization of the Hermitian symmetric space of $O(V)$, called the \emph{projective model}. This can easily be shown by proving that
\begin{displaymath}
  \HH_l \longrightarrow \Gr(V),\ Z=X + iY \longmapsto \RR X + \RR Y
\end{displaymath}
defines a real analytic isomorphism, see \cite[Lem. 2.17]{Bruinier2008}. We wrote $Z = X + iY$ for the decomposition of $Z \in V(\CC)$ into its real part $X$ and imaginary part $Y$.

\subsection{The tube domain model}
\label{sec:tube-domain-model}

The third model that will be of interest is the realization of the Hermitian symmetric space as a tube domain, which comes closest to the generalization of the upper half plane.

Let $e_{1} \in V_{\QQ}\setminus \{0\}$ be an isotropic vector and $e_{2} \in V_{\QQ}$ such that $(e_{1}, e_{2}) = 1$. Let $K$ be the sub lattice
\[ K = L \cap e_{1}^{\perp} \cap e_{2}^{\perp} \]
and similarly $W_{\QQ} = V_{\QQ} \cap e_{1}^{\perp} \cap e_{2}^{\perp} = K \otimes_{\ZZ} \QQ$ and $W = V \cap e_{1}^{\perp} \cap e_{2}^{\perp} = K \otimes \RR$. 
$W, W_{\QQ}$ and $K$ are Lorentzian, i.e., they have signature $(1, l-1)$. Obviously we have the decomposition $V = W \oplus \RR e_{1} \oplus \RR e_{2}$ and analogous decompositions for $V_{\QQ}$ and $V(\CC)$. Write $Z = (z,a,b)$ for this decomposition for a vector $Z \in V(\CC)$.
\begin{Def}\label{def:tube-domain-model}
  The \emph{tube domain model} of the Hermitian symmetric space is given by the set
  \[ \calH := \setb{z \in W(\CC)}{q(\Im z) > 0} \] together with the
  map
  \[ \calH \rightarrow \calK,\ z \mapsto \psi(z) := \big[ \big( z, 1, -q(z)-q(e_{2}) \big) \big]. \]
  This map is biholomorphic, see \cite[p.~138, Lem.~2.18]{Bruinier2008}
\end{Def}
If we define $\calP := \setb{y \in W}{q(y)>0}$ then $\calH = W + i\calP$. Observe that $\calP$ is a cone with two connected components, coming from the two cones of positive norm vectors in the Lorentzian space $W$. Write $\calP = \calP^{+} \cup \calP^{-}$ for this decomposition. Thus $\calH \subset W(\CC)$ has two connected components, which correspond to those of $\calK$. Let $\HH_{l}' = \psi^{-1}(\HH_{l})$ be the component of $\calH$ that gets mapped to $\HH_{l}$. the action of $O'(V)$ on $\HH_{l}'$, induced by the action of $O'(V)$ on  $\calK$, is transitive. However, it is not linear anymore, which is a disadvantage of the tube domain model.

It is worth mentioning, that this model comes closest to a \emph{generalized upper half plane}; for $l=1$, i.e., $O'(V) = O'(2,1)$, $\HH_{l}'$ can be identified with the usual upper half plane $\HH$ and the $O(2,2)$ case corresponds to $\HH \times \HH$.

\subsection{The discriminant kernel}
\label{sec:discriminant-kernel}

Let $O(L)$ be the orthogonal group of the lattice $L$,
\[ O(L) = \setb{\varphi \in O(V)}{\varphi L = L} \]
and write $O'(L) = O(L) \cap O'(V)$.
\begin{Def}
  The \emph{discriminant kernel} of $O'(L)$ is its subgroup $\Gamma_{L}$ that acts trivially on the discriminant group $L'/L$. We have the exact sequence
  \[ 1 \rightarrow \Gamma_{L} \rightarrow O'(L) \rightarrow \operatorname{Aut} (L'/L). \]
\end{Def}
We consider subgroups $\Gamma \subset \Gamma_{L}$ of finite index. Of particular interest will be the principal congruence subgroups $\Gamma_{L}(N)$ of $\Gamma_{L}$ of level $N$,
\[ \Gamma_{L}(N) = \Gamma_{L} \cap \ker ( O(V) \rightarrow O(L/NL) ). \]
The objects of study are the Picard groups of the quotients
\[ Y(\Gamma) := \Gamma \backslash \HH_{l}, \]
which are normal complex spaces. They are compact if and only if $V$ is anisotropic.

The set $Y(\Gamma)$ is a quasi-projective algebraic variety over $\CC$, according to the theory of Baily-Borel \cite{Baily1966}. Because $\Gamma$ acts properly discontinuously on $\HH_{l}$, $Y(\Gamma)$ can be compactified adding finitely many curves and points. One can also first compactify $\HH_{l}$ adding rational boundary components and then build the quotient by $\Gamma$. This compactification of $Y(\Gamma)$ is usually denoted by $X(\Gamma)$. For our purpose, it is not necessary to work with the compactification because $l \geq 3$ and the Koecher principle applies, see the discussion after Definition~\ref{def:entire_cusp}.

We will abbreviate $Y_{L}(N) := Y(\Gamma_{L}(N))$ and $Y_{L} := Y_{L}(1) = Y(\Gamma_{L})$.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "master"
%%% End: 
