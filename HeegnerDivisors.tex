
\section{Heegner divisors}
\label{sec:heegner-divisors}

Let $\lambda \in L'$ with $q(\lambda) < 0$. Then the orthogonal complement
\[ \lambda^{\perp} = \setb{[Z] \in \HH_{l}}{(Z,\lambda) = 0} = 
      \ker \big(\HH_{l} \xrightarrow{(\cdot,\lambda)} \CC \big) \]
of $\lambda$ in $\HH_{l}$ is a quadratic subspace with signature $(2,l-1)$. It is an analytic divisor, as it is defined in terms of the bilinear form. If $V_{\lambda} = \ker \big( V \xrightarrow{(\cdot,\lambda)} \CC \big)$ denotes the orthogonal complement of $\lambda$ in $V$, then $\lambda^{\perp} \subset \HH_{l}$ is the Hermitian symmetric space of $(V_{\lambda}, q|_{V_{\lambda}})$.
\begin{Def}\label{def:Heegner-divisor}
  Let $\beta \in L'/L$, $n \in \ZZ + q(\beta)$ with $n<0$ and $s_{\beta}=1$ if $2\beta \neq 0$, $s_{\beta}=2$ if $2 \beta = 0$. The \emph{Heegner divisor} of discriminant $(\beta,n)$ is defined by
  \begin{equation}
    \label{eqn:HeegnerDivDef}
    H(\beta,n) := \frac{1}{s_{\beta}} \sum_{\substack{\lambda \in \beta + L\\ q(\lambda) = n}} \lambda^{\perp}.
  \end{equation}
\end{Def}
It is a $\Gamma_{l}$-invariant divisor on $\HH_{l}$. For $\Gamma \subset \Gamma_{L}$ this divisor descents to an algebraic divisor on $Y(\Gamma)$ by Chow's Lemma, which we will denote in the same way. It is $H(\beta,n) = H(-\beta,n)$ and the multiplicities of all irreducible components of $H(\beta,n)$ are $1$, thanks to the normalization by $1/s_{\beta}$. This is a more natural definition for the purpose of this thesis, see the resulting Theorem~\ref{thm:result}. Mind that most references given here don't use this normalization.

Due to \cite[Thm.~4.23,~\pno~114, Cor.~4.24,~\pno~117]{Bruinier2002} there is a close relation between modular forms and Heegner divisors:
\begin{Thm}
  Let $F$ be a modular form of weight $k$ with some character for the group $\Gamma(L)$. As usual, let $C(\beta,n)$ denote the Fourier coefficients of the Eisenstein series $E(\tau)$. Suppose the divisor of $F$ is a linear combination of Heegner divisors
  \begin{equation*}
    \ddiv (F) = \frac{1}{2} \smashoperator[r]{\sum_{\beta \in L'/L}} \ \big( s_{\beta} \smashoperator{\sum_{\substack{n \in \ZZ + q(\beta) \\ n < 0}}} c(\beta,n) H(\beta,n) \big) .
  \end{equation*}
Then the weight is given by
\begin{equation}\label{eqn:weightHeegner}
  k = - \frac{1}{4} \sum_{\beta \in L'/L} \smashoperator[r]{\sum_{\substack{n \in \ZZ + q(\beta) \\ n < 0}}} c(\beta,n) C(\beta,-n).
\end{equation}
\end{Thm}
Now using Proposition~\ref{prop:singular-weights} the problem of finding non-trivial Heegner divisors could be tackled by finding a Heegner divisor ($c(\beta,n)=1$ for only one pair $(\beta,n)$ and $0$ otherwise) for which there cannot be a modular form of the weight given by \eqref{eqn:weightHeegner}, because the weight would be smaller than $s_{\beta}(l -2)$. But we already drew the connection between divisors and modular forms in Corollary~\ref{cor:divisorIneq}. The next Proposition draws a more general connection between the Fourier coefficients and Heegner divisors, which can be directly put into Proposition~\ref{prop:singular-weights}.
It was shown in \cite[Prop.~4.8,~\pno~24]{Bruinier2002a} and was the motivation for the evaluation of the Fourier coefficients.
\begin{Prop}\label{prop:degHeegnerCBeta_n}
  The degrees of Heegner divisors $H(\beta,-n)$ and the Fourier coefficients $C(\beta,n)$ of the Eisenstein series $E_{0}(\tau)$ are proportional, viz.
  \begin{equation}
    \label{eqn:degHeegnerCBeta_n}
    \deg (H(\beta,-n)) = - \frac{\vol(Y_{L})}{2 s_{\beta}} C(\beta,n).
  \end{equation}
\end{Prop}

We shall look at Heegner divisors of the form $H(\lambda,q(\lambda))$ for $\lambda \in L'$ with negative norm. In view of \ref{cor:divisorIneq} the proposition yields a criterion for the non-triviality of a Heegner divisor and thus of the Picard group this Heegner divisor is an element of:
\begin{Cor}\label{cor:HeegnerNonTriv}
  Suppose the coefficient $C(\lambda,-q(\lambda))$ satisfies
  \begin{equation}
    \label{eqn:CLambdaCor}
    - C(\lambda,-q(\lambda)) < s_{\lambda} (l-2),
  \end{equation}
  then $H(\lambda,(q(\lambda))$ defines a non-trivial element of $\Pic_{\QQ}(Y_{L})$.
\end{Cor}
\begin{proof}
  If the line bundle corresponding to $H (\lambda,q(\lambda))$ lies in the span of the canonical bundle $\calC$, then
\[ \deg (H(\lambda,q(\lambda))) \geq (l/2 -1) \vol (Y_{L}) \]
according to \ref{cor:divisorIneq}. By the last Proposition~\ref{prop:degHeegnerCBeta_n} this reads
\begin{IEEEeqnarray*}{rCl}
  - \frac{\vol(Y_{L})}{2 s_{\lambda}} C(\lambda, - q(\lambda)) & \geq &
  (l/2 - 1) \vol (Y_{L}) \text{, i.e.,}\\
- C (\lambda, -q(\lambda)) & \geq & s_{\lambda} (l-2).
\end{IEEEeqnarray*}
So if the hypotheses~\eqref{eqn:CLambdaCor} is true, $\calL( H(\lambda,q (\lambda)))$ cannot lie in $\calC$, i.e., $H(\lambda,q(\lambda))$ is non-trivial.
\end{proof}

This corollary will be the basis for the upcoming calculations. In order to show the non-triviality of the Picard group, we would like to minimize $0 \leq - C(\lambda, -q(\lambda))$, i.e., find a vector $\lambda \in L'$ for which $- C(\lambda, -q(\lambda))$ is particularly small.